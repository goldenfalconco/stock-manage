﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStockPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stocks", "Itemprice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stocks", "Itemprice");
        }
    }
}
