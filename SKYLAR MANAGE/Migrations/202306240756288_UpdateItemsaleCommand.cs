﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateItemsaleCommand : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomerTransactionItem", "IsCommand", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomerTransactionItem", "IsCommand");
        }
    }
}
