﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Barcodes",
                c => new
                    {
                        Barcodeid = c.Int(nullable: false, identity: true),
                        BarcodeValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Barcodeid);
            
            CreateTable(
                "dbo.CustomersAccounts",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(),
                        CustomerPhone = c.String(),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Privilege",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        NameEn = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        ItemBarcode = c.String(nullable: false, maxLength: 128),
                        ItemName = c.String(),
                        ItemAvailableQty = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ItemBarcode);
            
            CreateTable(
                "dbo.SupplierTransactionItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.String(),
                        InvoiceAutoNbId = c.Int(nullable: false),
                        Qty = c.Double(nullable: false),
                        Notes = c.String(),
                        Invoice_AutoNbr = c.Int(),
                        Product_ItemBarcode = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SupplierTransactions", t => t.Invoice_AutoNbr)
                .ForeignKey("dbo.Stocks", t => t.Product_ItemBarcode)
                .Index(t => t.Invoice_AutoNbr)
                .Index(t => t.Product_ItemBarcode);
            
            CreateTable(
                "dbo.SupplierTransactions",
                c => new
                    {
                        AutoNbr = c.Int(nullable: false, identity: true),
                        SupplierId = c.Int(nullable: false),
                        TranTypeId = c.Int(nullable: false),
                        Notes = c.String(),
                        UpdatedBy = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AutoNbr)
                .ForeignKey("dbo.SuppliersAccounts", t => t.SupplierId, cascadeDelete: true)
                .ForeignKey("dbo.TransactionTypes", t => t.TranTypeId, cascadeDelete: true)
                .Index(t => t.SupplierId)
                .Index(t => t.TranTypeId);
            
            CreateTable(
                "dbo.SuppliersAccounts",
                c => new
                    {
                        SupplierId = c.Int(nullable: false, identity: true),
                        SupplierName = c.String(),
                        SupplierPhone = c.String(),
                    })
                .PrimaryKey(t => t.SupplierId);
            
            CreateTable(
                "dbo.TransactionTypes",
                c => new
                    {
                        TranTypeId = c.Int(nullable: false, identity: true),
                        TranTypeDesc = c.String(),
                    })
                .PrimaryKey(t => t.TranTypeId);
            
            CreateTable(
                "dbo.UsersAccounts",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        UserPassword = c.String(),
                        Active = c.Boolean(),
                        Privileges = c.String(unicode: false, storeType: "text"),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.UsersPcInfoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        PcIp = c.String(),
                        PcName = c.String(),
                        AppVersion = c.String(),
                        WindowsVersion = c.String(),
                        MacAddress = c.String(),
                        LastLogin = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierTransactionItem", "Product_ItemBarcode", "dbo.Stocks");
            DropForeignKey("dbo.SupplierTransactions", "TranTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.SupplierTransactions", "SupplierId", "dbo.SuppliersAccounts");
            DropForeignKey("dbo.SupplierTransactionItem", "Invoice_AutoNbr", "dbo.SupplierTransactions");
            DropIndex("dbo.SupplierTransactions", new[] { "TranTypeId" });
            DropIndex("dbo.SupplierTransactions", new[] { "SupplierId" });
            DropIndex("dbo.SupplierTransactionItem", new[] { "Product_ItemBarcode" });
            DropIndex("dbo.SupplierTransactionItem", new[] { "Invoice_AutoNbr" });
            DropTable("dbo.UsersPcInfoes");
            DropTable("dbo.UsersAccounts");
            DropTable("dbo.TransactionTypes");
            DropTable("dbo.SuppliersAccounts");
            DropTable("dbo.SupplierTransactions");
            DropTable("dbo.SupplierTransactionItem");
            DropTable("dbo.Stocks");
            DropTable("dbo.Privilege");
            DropTable("dbo.CustomersAccounts");
            DropTable("dbo.Barcodes");
        }
    }
}
