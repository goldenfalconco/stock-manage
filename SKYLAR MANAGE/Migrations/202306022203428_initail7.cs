﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initail7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomersAccounts", "TransactionTypes_TranTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.CustomersAccounts", new[] { "TransactionTypes_TranTypeId" });
            AddColumn("dbo.CustomersAccounts", "DepartmentId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomersAccounts", "Department_DepId", c => c.Int());
            CreateIndex("dbo.CustomersAccounts", "Department_DepId");
            AddForeignKey("dbo.CustomersAccounts", "Department_DepId", "dbo.Departments", "DepId");
            DropColumn("dbo.CustomersAccounts", "TransactionTypes_TranTypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomersAccounts", "TransactionTypes_TranTypeId", c => c.Int());
            DropForeignKey("dbo.CustomersAccounts", "Department_DepId", "dbo.Departments");
            DropIndex("dbo.CustomersAccounts", new[] { "Department_DepId" });
            DropColumn("dbo.CustomersAccounts", "Department_DepId");
            DropColumn("dbo.CustomersAccounts", "DepartmentId");
            CreateIndex("dbo.CustomersAccounts", "TransactionTypes_TranTypeId");
            AddForeignKey("dbo.CustomersAccounts", "TransactionTypes_TranTypeId", "dbo.TransactionTypes", "TranTypeId");
        }
    }
}
