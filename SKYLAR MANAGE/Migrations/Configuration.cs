﻿using SKYLAR_MANAGE.Functions;

namespace SKYLAR_MANAGE.Migrations
{
    using CodeTecLib;
    using SKYLAR_MANAGE.Models;
    using SKYLAR_MANAGE.Tools;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SKYLAR_MANAGE.MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            
        }

        protected override void Seed(SKYLAR_MANAGE.MyDbContext context)
        {
            var password = CodeTecCodec.Base64Encode("Admin121097D");

            // Add default user
          //  context.UsersAccounts.AddIfNotExists(new UsersAccounts { Username = "admin", UserPassword = password, Active = true }, x => x.UserId == 1);

            // add transaction Types
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Purchase Invoice" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Return Purchase" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Adjust In" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Purchase Order" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Sale Invoice" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Return Sales" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Adjust Out" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Sale Order" });
            context.TransactionTypes.AddIfNotExists(new TransactionTypes { TranTypeDesc = "Opening QTY" });

            // add Main barcode
            context.Barcodes.AddIfNotExists(new Barcode { BarcodeValue = 88000 });

            // add privilage
            var listPrivileges = new List<Privilege>
            {
                // admin Permissions 100
             
                new() { ID = (int)MyTypes.Privilege.ViewUsers, NameEn = "View Users Accounts"},

            };

            listPrivileges.ForEach(s => { context.Privilege.AddIfNotExists(s, x => x.ID == s.ID); });

            // add Departments

            context.Departments.AddIfNotExists(new Department { DepName = "Chambre Froid" });
            context.Departments.AddIfNotExists(new Department { DepName = "Garage 12eme" });
            context.Departments.AddIfNotExists(new Department { DepName = "Garage 17eme" });
            context.Departments.AddIfNotExists(new Department { DepName = "Plastic" });
            context.Departments.AddIfNotExists(new Department { DepName = "Martdella" });
            context.Departments.AddIfNotExists(new Department { DepName = "Pannaux" });
            context.Departments.AddIfNotExists(new Department { DepName = "Electrical Maintenance" });
            context.Departments.AddIfNotExists(new Department { DepName = "Froid Maintenance" });
            context.Departments.AddIfNotExists(new Department { DepName = "La Prima" });
            context.Departments.AddIfNotExists(new Department { DepName = "FERME Maluku" });
            context.Departments.AddIfNotExists(new Department { DepName = "Fasol Kinshasa" });
            context.Departments.AddIfNotExists(new Department { DepName = "Mouilino" });
            context.Departments.AddIfNotExists(new Department { DepName = "Adminisitrations" });
        }
    }
}
