﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomersAccounts", "TransactionTypes_TranTypeId", c => c.Int());
            CreateIndex("dbo.CustomersAccounts", "TransactionTypes_TranTypeId");
            AddForeignKey("dbo.CustomersAccounts", "TransactionTypes_TranTypeId", "dbo.TransactionTypes", "TranTypeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomersAccounts", "TransactionTypes_TranTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.CustomersAccounts", new[] { "TransactionTypes_TranTypeId" });
            DropColumn("dbo.CustomersAccounts", "TransactionTypes_TranTypeId");
        }
    }
}
