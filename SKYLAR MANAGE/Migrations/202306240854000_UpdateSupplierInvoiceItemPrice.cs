﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSupplierInvoiceItemPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SupplierTransactionItem", "Itemprice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SupplierTransactionItem", "Itemprice");
        }
    }
}
