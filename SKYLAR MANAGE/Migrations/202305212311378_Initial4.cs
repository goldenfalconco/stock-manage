﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepId = c.Int(nullable: false, identity: true),
                        DepName = c.String(),
                    })
                .PrimaryKey(t => t.DepId);
            
            AddColumn("dbo.CustomerTransactions", "DepartmentId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomerTransactions", "Department_DepId", c => c.Int());
            CreateIndex("dbo.CustomerTransactions", "Department_DepId");
            AddForeignKey("dbo.CustomerTransactions", "Department_DepId", "dbo.Departments", "DepId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomerTransactions", "Department_DepId", "dbo.Departments");
            DropIndex("dbo.CustomerTransactions", new[] { "Department_DepId" });
            DropColumn("dbo.CustomerTransactions", "Department_DepId");
            DropColumn("dbo.CustomerTransactions", "DepartmentId");
            DropTable("dbo.Departments");
        }
    }
}
