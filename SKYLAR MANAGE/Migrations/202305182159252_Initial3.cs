﻿namespace SKYLAR_MANAGE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerTransactionItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.String(),
                        InvoiceAutoNbId = c.Int(nullable: false),
                        Qty = c.Double(nullable: false),
                        Notes = c.String(),
                        Invoice_AutoNbr = c.Int(),
                        Product_ItemBarcode = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomerTransactions", t => t.Invoice_AutoNbr)
                .ForeignKey("dbo.Stocks", t => t.Product_ItemBarcode)
                .Index(t => t.Invoice_AutoNbr)
                .Index(t => t.Product_ItemBarcode);
            
            CreateTable(
                "dbo.CustomerTransactions",
                c => new
                    {
                        AutoNbr = c.Int(nullable: false, identity: true),
                        CustomerId = c.Int(nullable: false),
                        TranTypeId = c.Int(nullable: false),
                        Notes = c.String(),
                        UpdatedBy = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AutoNbr)
                .ForeignKey("dbo.CustomersAccounts", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.TransactionTypes", t => t.TranTypeId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.TranTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomerTransactionItem", "Product_ItemBarcode", "dbo.Stocks");
            DropForeignKey("dbo.CustomerTransactions", "TranTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.CustomerTransactionItem", "Invoice_AutoNbr", "dbo.CustomerTransactions");
            DropForeignKey("dbo.CustomerTransactions", "CustomerId", "dbo.CustomersAccounts");
            DropIndex("dbo.CustomerTransactions", new[] { "TranTypeId" });
            DropIndex("dbo.CustomerTransactions", new[] { "CustomerId" });
            DropIndex("dbo.CustomerTransactionItem", new[] { "Product_ItemBarcode" });
            DropIndex("dbo.CustomerTransactionItem", new[] { "Invoice_AutoNbr" });
            DropTable("dbo.CustomerTransactions");
            DropTable("dbo.CustomerTransactionItem");
        }
    }
}
