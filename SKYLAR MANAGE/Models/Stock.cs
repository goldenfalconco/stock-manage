namespace SKYLAR_MANAGE.Models
{
    using SKYLAR_MANAGE.Screens.Stock;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class Stock
    {
        public Stock()
        {
           
            Item = new HashSet<SupplierTransactionItem>();
           
        }


       
       
        [Key] public string ItemBarcode { get; set; }

        public string ItemName { get; set; }

        public double ItemAvailableQty { get; set; }

        public double Itemprice { get; set; }


        public ICollection<SupplierTransactionItem> Item { get; set; }
    }
}
