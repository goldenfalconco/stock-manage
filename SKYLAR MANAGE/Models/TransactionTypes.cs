﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKYLAR_MANAGE.Models
{
    public class TransactionTypes
    {
        [Key]
        public int TranTypeId { get; set; }
        public string TranTypeDesc { get; set; }

    }
}
