﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKYLAR_MANAGE.Models
{
    public class SuppliersAccount
    {
        [Key]
        public int SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string SupplierPhone { get; set; }

    }
}
