using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKYLAR_MANAGE.Models
{
    [Table("Privilege")]
    public class Privilege
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [StringLength(255)] public string NameEn { get; set; }

    }
}