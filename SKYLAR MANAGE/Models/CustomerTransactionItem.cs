using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKYLAR_MANAGE.Models
{
    [Table("CustomerTransactionItem")]
    public class CustomerTransactionItem
    {
        [Key] public int Id { get; set; }

        
        public string ProductId { get; set; }
        public virtual Stock Product { get; set; }
        public int InvoiceAutoNbId { get; set; }
        public virtual CustomerTransaction Invoice { get; set; }

        public double Qty { get; set; }
        public bool IsCommand { get; set; }
        public string Notes { get; set; }

    }
}