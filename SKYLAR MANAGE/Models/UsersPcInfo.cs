namespace SKYLAR_MANAGE.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class UsersPcInfo
    {
        public int id { get; set; }

        public string PcIp { get; set; }

        public string PcName { get; set; }

        public string AppVersion { get; set; }

        public string WindowsVersion { get; set; }

        public string MacAddress { get; set; }

     

        public DateTime? LastLogin { get; set; }
    }
}
