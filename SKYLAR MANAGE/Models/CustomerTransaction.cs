﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKYLAR_MANAGE.Models
{
    public class CustomerTransaction

    {
        public CustomerTransaction()
        {
            Item = new HashSet<CustomerTransactionItem>();
        }
        [Key]
        public int AutoNbr { get; set; }

        public int CustomerId { get; set; }

        public int TranTypeId { get; set; }

        public int DepartmentId { get; set; }
        public string Notes { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Date { get; set; }

       
        public virtual CustomersAccount CustomersAccount { get; set; }
        public virtual TransactionTypes TransactionTypes { get; set; }

        public virtual Department Department { get; set; }
        public ICollection<CustomerTransactionItem> Item { get; set; }


    }
}
