﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKYLAR_MANAGE.Models
{
    public class SupplierTransaction

    {
        public SupplierTransaction()
        {
            Item = new HashSet<SupplierTransactionItem>();
        }
        [Key]
        public int AutoNbr { get; set; }

        public int SupplierId { get; set; }

        public int TranTypeId { get; set; }
        public string Notes { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Date { get; set; }

        public virtual SuppliersAccount SuppliersAccount { get; set; }
        public virtual TransactionTypes TransactionTypes { get; set; }
        public ICollection<SupplierTransactionItem> Item { get; set; }


    }
}
