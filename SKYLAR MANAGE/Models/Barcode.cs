namespace SKYLAR_MANAGE.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class Barcode
    {
        [Key]
        public int Barcodeid { get; set; }
        public  int BarcodeValue { get; set; }

    }
}
