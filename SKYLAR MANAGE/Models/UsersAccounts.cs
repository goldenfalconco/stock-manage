﻿using DevExpress.DashboardCommon.Native;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKYLAR_MANAGE.Models
{
    public class UsersAccounts
    {
        [Key]
        public int UserId { get; set; }

        public string Username { get; set; }

        public string UserPassword { get; set; }
        public bool? Active { get; set; }
        [Column(TypeName = "text")] public string Privileges { get; set; }

    

    }
}
