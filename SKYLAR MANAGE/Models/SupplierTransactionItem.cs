using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SKYLAR_MANAGE.Models
{
    [Table("SupplierTransactionItem")]
    public class SupplierTransactionItem
    {
        [Key] public int Id { get; set; }

        
        public string ProductId { get; set; }
        public virtual Stock Product { get; set; }
        public int InvoiceAutoNbId { get; set; }
        public virtual SupplierTransaction Invoice { get; set; }
        public double Itemprice { get; set; }
        public double Qty { get; set; }
        public string Notes { get; set; }

    }
}