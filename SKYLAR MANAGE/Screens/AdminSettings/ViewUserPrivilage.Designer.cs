﻿namespace SKYLAR_MANAGE.Screens.AdminSettings
{
    partial class ViewUserPrivilage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabContactLogin = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.userpassword = new DevExpress.XtraEditors.TextEdit();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUsername = new DevExpress.XtraEditors.TextEdit();
            this.Savebtn = new System.Windows.Forms.Button();
            this.tblAllPrivileges = new System.Windows.Forms.TableLayoutPanel();
            this.flwCompany = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblSuppliers = new System.Windows.Forms.Label();
            this.flwSuppplier = new System.Windows.Forms.FlowLayoutPanel();
            this.lblStock = new System.Windows.Forms.Label();
            this.flwStock = new System.Windows.Forms.FlowLayoutPanel();
            this.lblConsumer = new System.Windows.Forms.Label();
            this.flwConsumers = new System.Windows.Forms.FlowLayoutPanel();
            this.lblReports = new System.Windows.Forms.Label();
            this.flwReports = new System.Windows.Forms.FlowLayoutPanel();
            this.lbl6 = new System.Windows.Forms.Label();
            this.flw6 = new System.Windows.Forms.FlowLayoutPanel();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lb9 = new System.Windows.Forms.Label();
            this.flw7 = new System.Windows.Forms.FlowLayoutPanel();
            this.flw8 = new System.Windows.Forms.FlowLayoutPanel();
            this.flw9 = new System.Windows.Forms.FlowLayoutPanel();
            this.tabControl1.SuspendLayout();
            this.tabContactLogin.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userpassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).BeginInit();
            this.tblAllPrivileges.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabContactLogin);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1440, 77);
            this.tabControl1.TabIndex = 120;
            this.tabControl1.TabStop = false;
            // 
            // tabContactLogin
            // 
            this.tabContactLogin.BackColor = System.Drawing.Color.White;
            this.tabContactLogin.Controls.Add(this.tableLayoutPanel1);
            this.tabContactLogin.Location = new System.Drawing.Point(4, 25);
            this.tabContactLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabContactLogin.Name = "tabContactLogin";
            this.tabContactLogin.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabContactLogin.Size = new System.Drawing.Size(1432, 48);
            this.tabContactLogin.TabIndex = 0;
            this.tabContactLogin.Text = "Login Information";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28531F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28816F));
            this.tableLayoutPanel1.Controls.Add(this.userpassword, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkSelectAll, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkActive, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblPassword, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblUsername, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtUsername, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Savebtn, 6, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1426, 40);
            this.tableLayoutPanel1.TabIndex = 112;
            // 
            // userpassword
            // 
            this.userpassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.userpassword.Location = new System.Drawing.Point(612, 9);
            this.userpassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.userpassword.Name = "userpassword";
            this.userpassword.Properties.ReadOnly = true;
            this.userpassword.Size = new System.Drawing.Size(197, 22);
            this.userpassword.TabIndex = 113;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.chkSelectAll.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.chkSelectAll.ForeColor = System.Drawing.Color.White;
            this.chkSelectAll.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkSelectAll.Location = new System.Drawing.Point(1018, 9);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(197, 21);
            this.chkSelectAll.TabIndex = 6;
            this.chkSelectAll.Text = "Select All Privilage";
            this.chkSelectAll.UseVisualStyleBackColor = false;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // chkActive
            // 
            this.chkActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkActive.AutoSize = true;
            this.chkActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.chkActive.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.chkActive.ForeColor = System.Drawing.Color.White;
            this.chkActive.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkActive.Location = new System.Drawing.Point(815, 9);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(197, 21);
            this.chkActive.TabIndex = 5;
            this.chkActive.Text = "Active";
            this.chkActive.UseVisualStyleBackColor = false;
            // 
            // lblPassword
            // 
            this.lblPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPassword.AutoSize = true;
            this.lblPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblPassword.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.ForeColor = System.Drawing.Color.White;
            this.lblPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPassword.Location = new System.Drawing.Point(409, 11);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(197, 18);
            this.lblPassword.TabIndex = 111;
            this.lblPassword.Text = "Password";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUsername
            // 
            this.lblUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUsername.AutoSize = true;
            this.lblUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblUsername.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.White;
            this.lblUsername.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblUsername.Location = new System.Drawing.Point(3, 11);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(197, 18);
            this.lblUsername.TabIndex = 111;
            this.lblUsername.Text = "Username";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsername.Location = new System.Drawing.Point(206, 9);
            this.txtUsername.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Properties.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(197, 22);
            this.txtUsername.TabIndex = 2;
            // 
            // Savebtn
            // 
            this.Savebtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Savebtn.BackColor = System.Drawing.Color.Peru;
            this.Savebtn.Font = new System.Drawing.Font("Tahoma", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Savebtn.Location = new System.Drawing.Point(1221, 3);
            this.Savebtn.Name = "Savebtn";
            this.Savebtn.Size = new System.Drawing.Size(202, 34);
            this.Savebtn.TabIndex = 112;
            this.Savebtn.Text = "Save";
            this.Savebtn.UseVisualStyleBackColor = false;
            this.Savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // tblAllPrivileges
            // 
            this.tblAllPrivileges.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tblAllPrivileges.ColumnCount = 3;
            this.tblAllPrivileges.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblAllPrivileges.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblAllPrivileges.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblAllPrivileges.Controls.Add(this.flwCompany, 0, 1);
            this.tblAllPrivileges.Controls.Add(this.lblCompany, 0, 0);
            this.tblAllPrivileges.Controls.Add(this.lblSuppliers, 1, 0);
            this.tblAllPrivileges.Controls.Add(this.flwSuppplier, 1, 1);
            this.tblAllPrivileges.Controls.Add(this.lblStock, 2, 0);
            this.tblAllPrivileges.Controls.Add(this.flwStock, 2, 1);
            this.tblAllPrivileges.Controls.Add(this.lblConsumer, 0, 2);
            this.tblAllPrivileges.Controls.Add(this.flwConsumers, 0, 3);
            this.tblAllPrivileges.Controls.Add(this.lblReports, 1, 2);
            this.tblAllPrivileges.Controls.Add(this.flwReports, 1, 3);
            this.tblAllPrivileges.Controls.Add(this.lbl6, 2, 2);
            this.tblAllPrivileges.Controls.Add(this.flw6, 2, 3);
            this.tblAllPrivileges.Controls.Add(this.lbl7, 0, 4);
            this.tblAllPrivileges.Controls.Add(this.lbl8, 1, 4);
            this.tblAllPrivileges.Controls.Add(this.lb9, 2, 4);
            this.tblAllPrivileges.Controls.Add(this.flw7, 0, 5);
            this.tblAllPrivileges.Controls.Add(this.flw8, 1, 5);
            this.tblAllPrivileges.Controls.Add(this.flw9, 2, 5);
            this.tblAllPrivileges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblAllPrivileges.Location = new System.Drawing.Point(0, 77);
            this.tblAllPrivileges.Name = "tblAllPrivileges";
            this.tblAllPrivileges.RowCount = 6;
            this.tblAllPrivileges.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tblAllPrivileges.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblAllPrivileges.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tblAllPrivileges.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblAllPrivileges.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tblAllPrivileges.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblAllPrivileges.Size = new System.Drawing.Size(1440, 566);
            this.tblAllPrivileges.TabIndex = 121;
            // 
            // flwCompany
            // 
            this.flwCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flwCompany.AutoScroll = true;
            this.flwCompany.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwCompany.Location = new System.Drawing.Point(6, 44);
            this.flwCompany.Name = "flwCompany";
            this.flwCompany.Size = new System.Drawing.Size(470, 140);
            this.flwCompany.TabIndex = 126;
            // 
            // lblCompany
            // 
            this.lblCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCompany.AutoSize = true;
            this.lblCompany.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lblCompany.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblCompany.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCompany.Location = new System.Drawing.Point(6, 10);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(470, 21);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "Company";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSuppliers
            // 
            this.lblSuppliers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSuppliers.AutoSize = true;
            this.lblSuppliers.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lblSuppliers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblSuppliers.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblSuppliers.Location = new System.Drawing.Point(485, 3);
            this.lblSuppliers.Name = "lblSuppliers";
            this.lblSuppliers.Size = new System.Drawing.Size(470, 35);
            this.lblSuppliers.TabIndex = 0;
            this.lblSuppliers.Text = "Suppliers";
            this.lblSuppliers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flwSuppplier
            // 
            this.flwSuppplier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flwSuppplier.AutoScroll = true;
            this.flwSuppplier.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwSuppplier.Location = new System.Drawing.Point(485, 44);
            this.flwSuppplier.Name = "flwSuppplier";
            this.flwSuppplier.Size = new System.Drawing.Size(470, 140);
            this.flwSuppplier.TabIndex = 0;
            // 
            // lblStock
            // 
            this.lblStock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStock.AutoSize = true;
            this.lblStock.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lblStock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblStock.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStock.Location = new System.Drawing.Point(964, 3);
            this.lblStock.Name = "lblStock";
            this.lblStock.Size = new System.Drawing.Size(470, 35);
            this.lblStock.TabIndex = 0;
            this.lblStock.Text = "Stock";
            this.lblStock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flwStock
            // 
            this.flwStock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flwStock.AutoScroll = true;
            this.flwStock.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwStock.Location = new System.Drawing.Point(964, 44);
            this.flwStock.Name = "flwStock";
            this.flwStock.Size = new System.Drawing.Size(470, 140);
            this.flwStock.TabIndex = 0;
            // 
            // lblConsumer
            // 
            this.lblConsumer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblConsumer.AutoSize = true;
            this.lblConsumer.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lblConsumer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblConsumer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblConsumer.Location = new System.Drawing.Point(6, 190);
            this.lblConsumer.Name = "lblConsumer";
            this.lblConsumer.Size = new System.Drawing.Size(470, 35);
            this.lblConsumer.TabIndex = 0;
            this.lblConsumer.Text = "Consumers";
            this.lblConsumer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flwConsumers
            // 
            this.flwConsumers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flwConsumers.AutoScroll = true;
            this.flwConsumers.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwConsumers.Location = new System.Drawing.Point(6, 231);
            this.flwConsumers.Name = "flwConsumers";
            this.flwConsumers.Size = new System.Drawing.Size(470, 140);
            this.flwConsumers.TabIndex = 126;
            // 
            // lblReports
            // 
            this.lblReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReports.AutoSize = true;
            this.lblReports.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lblReports.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lblReports.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblReports.Location = new System.Drawing.Point(485, 190);
            this.lblReports.Name = "lblReports";
            this.lblReports.Size = new System.Drawing.Size(470, 35);
            this.lblReports.TabIndex = 0;
            this.lblReports.Text = "Reports";
            this.lblReports.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flwReports
            // 
            this.flwReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flwReports.AutoScroll = true;
            this.flwReports.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwReports.Location = new System.Drawing.Point(485, 231);
            this.flwReports.Name = "flwReports";
            this.flwReports.Size = new System.Drawing.Size(470, 140);
            this.flwReports.TabIndex = 0;
            // 
            // lbl6
            // 
            this.lbl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl6.AutoSize = true;
            this.lbl6.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lbl6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lbl6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl6.Location = new System.Drawing.Point(964, 190);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(470, 35);
            this.lbl6.TabIndex = 0;
            this.lbl6.Text = "lbl6";
            this.lbl6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flw6
            // 
            this.flw6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flw6.AutoScroll = true;
            this.flw6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flw6.Location = new System.Drawing.Point(964, 231);
            this.flw6.Name = "flw6";
            this.flw6.Size = new System.Drawing.Size(470, 140);
            this.flw6.TabIndex = 0;
            // 
            // lbl7
            // 
            this.lbl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lbl7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lbl7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl7.Location = new System.Drawing.Point(6, 377);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(470, 35);
            this.lbl7.TabIndex = 0;
            this.lbl7.Text = "lbl7";
            this.lbl7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl8
            // 
            this.lbl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl8.AutoSize = true;
            this.lbl8.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lbl8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lbl8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl8.Location = new System.Drawing.Point(485, 377);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(470, 35);
            this.lbl8.TabIndex = 0;
            this.lbl8.Text = "lbl8";
            this.lbl8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb9
            // 
            this.lb9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb9.AutoSize = true;
            this.lb9.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.lb9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.lb9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lb9.Location = new System.Drawing.Point(964, 377);
            this.lb9.Name = "lb9";
            this.lb9.Size = new System.Drawing.Size(470, 35);
            this.lb9.TabIndex = 0;
            this.lb9.Text = "lbl9";
            this.lb9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flw7
            // 
            this.flw7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flw7.AutoScroll = true;
            this.flw7.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flw7.Location = new System.Drawing.Point(6, 418);
            this.flw7.Name = "flw7";
            this.flw7.Size = new System.Drawing.Size(470, 142);
            this.flw7.TabIndex = 0;
            // 
            // flw8
            // 
            this.flw8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flw8.AutoScroll = true;
            this.flw8.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flw8.Location = new System.Drawing.Point(485, 418);
            this.flw8.Name = "flw8";
            this.flw8.Size = new System.Drawing.Size(470, 142);
            this.flw8.TabIndex = 0;
            // 
            // flw9
            // 
            this.flw9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flw9.AutoScroll = true;
            this.flw9.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flw9.Location = new System.Drawing.Point(964, 418);
            this.flw9.Name = "flw9";
            this.flw9.Size = new System.Drawing.Size(470, 142);
            this.flw9.TabIndex = 0;
            // 
            // ViewUserPrivilage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 643);
            this.Controls.Add(this.tblAllPrivileges);
            this.Controls.Add(this.tabControl1);
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.Name = "ViewUserPrivilage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View User Privilage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ViewUserPrivilage_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabContactLogin.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userpassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsername.Properties)).EndInit();
            this.tblAllPrivileges.ResumeLayout(false);
            this.tblAllPrivileges.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabContactLogin;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox chkSelectAll;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblUsername;
        private DevExpress.XtraEditors.TextEdit txtUsername;
        private System.Windows.Forms.TableLayoutPanel tblAllPrivileges;
        private System.Windows.Forms.FlowLayoutPanel flwCompany;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label lblSuppliers;
        private System.Windows.Forms.FlowLayoutPanel flwSuppplier;
        private System.Windows.Forms.Label lblStock;
        private System.Windows.Forms.FlowLayoutPanel flwStock;
        private System.Windows.Forms.Label lblConsumer;
        private System.Windows.Forms.FlowLayoutPanel flwConsumers;
        private System.Windows.Forms.Label lblReports;
        private System.Windows.Forms.FlowLayoutPanel flwReports;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.FlowLayoutPanel flw6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lb9;
        private System.Windows.Forms.FlowLayoutPanel flw7;
        private System.Windows.Forms.FlowLayoutPanel flw8;
        private System.Windows.Forms.FlowLayoutPanel flw9;
        private System.Windows.Forms.Button Savebtn;
        private DevExpress.XtraEditors.TextEdit userpassword;
    }
}