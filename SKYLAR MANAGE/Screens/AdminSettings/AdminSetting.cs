﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeTecLib;
using System.Diagnostics;
using DevExpress.XtraPrinting;
using DevExpress.Export;
using DevExpress.XtraGrid;
using DevExpress.Printing.ExportHelpers;
using DevExpress.XtraGrid.Views.Grid;
using SKYLAR_MANAGE.Screens.AdminSettings;
using SKYLAR_MANAGE.Screens.Stock;
using SKYLAR_MANAGE.Screens.Suppliers;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class AdminSetting : DevExpress.XtraEditors.XtraForm
    {
        private string UserID = "";
        public AdminSetting()
        {
            InitializeComponent();
            MyDbContext ctx = new MyDbContext();
            usersAccountsBindingSource.DataSource = ctx.UsersAccounts.ToList();

        }



        private void exportbtn_Click(object sender, EventArgs e)
        {
            // Ensure that the data-aware export mode is enabled.
            DevExpress.Export.ExportSettings.DefaultExportType = ExportType.DataAware;
            // Create a new object defining how a document is exported to the XLSX format.
            XlsxExportOptionsEx options = new XlsxExportOptionsEx();
            // Subscribe to the CustomizeSheetHeader event. 
            options.CustomizeSheetHeader += options_CustomizeSheetHeader;
            // Export the grid data to the XLSX format.
            string file = "UsersAccounts.xlsx";
            gridControl1.ExportToXlsx(file, options);
            // Open the created document.
            System.Diagnostics.Process.Start(file);
        }

        void options_CustomizeSheetHeader(DevExpress.Export.ContextEventArgs e)
        {
            // Create a new row.
            CellObject row = new CellObject();
            // Specify row values.
            row.Value = "The document is exported from the IssueList database.";
            // Specify row formatting.
            XlFormattingObject rowFormatting = new XlFormattingObject();
            rowFormatting.Font = new XlCellFont { Bold = true, Size = 14 };
            rowFormatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment
            {
                HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center,
                VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top
            };
            row.Formatting = rowFormatting;
            // Add the created row to the output document.
            e.ExportContext.AddRow(new[] { row });
            // Add an empty row to the output document.
            e.ExportContext.AddRow();
            // Merge cells of two new rows. 
            e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(
                new DevExpress.Export.Xl.XlCellPosition(0, 0), new DevExpress.Export.Xl.XlCellPosition(5, 1)));
        }

        private void adduserbtn_Click(object sender, EventArgs e)
        {
            Boolean frmopen = false;
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                //iterate through
                if (frm.Name == "new_user")
                {
                    frmopen = true;
                }

            }

            if (!frmopen)
            {
                new_user nu = new new_user();
                nu.ShowDialog();
                refreshbtn_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
            }
        }

        private void refreshbtn_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            usersAccountsBindingSource.DataSource = ctx.UsersAccounts.ToList();
            UserID = "";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.UsersAccounts user)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "EditUser")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    EditUser ui = new EditUser(user.UserId.ToString());
                    ui.ShowDialog();
                    refreshbtn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
           
        }

      

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.UsersAccounts user)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "ViewUserPrivilage")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    ViewUserPrivilage ui = new ViewUserPrivilage(user.UserId.ToString());
                    ui.ShowDialog();
                    refreshbtn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
            
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {

            try
            {
                if (gridView1.GetFocusedRow() is not Models.UsersAccounts User)
                    return;
                Boolean frmopen = false;
                    FormCollection fc = Application.OpenForms;

                    foreach (Form frm in fc)
                    {
                        //iterate through
                        if (frm.Name == "ViewUserPrivilage")
                        {
                            frmopen = true;
                        }

                    }

                    if (!frmopen)
                    {
                        ViewUserPrivilage ui = new ViewUserPrivilage(User.UserId.ToString());
                        ui.ShowDialog();
                        refreshbtn_Click(sender, e);
                    }
                    else
                    {
                        MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                
              
            }
            catch (Exception ex)
            {

            }
           
        }
    }
}