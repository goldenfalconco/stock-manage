﻿namespace SKYLAR_MANAGE.Screens.AdminSettings
{
    partial class EditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.UserPass_txt = new System.Windows.Forms.TextBox();
            this.UserName_txt = new System.Windows.Forms.TextBox();
            this.UserName_lbl = new System.Windows.Forms.Label();
            this.Passlbel = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(33, 224);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(452, 49);
            this.btnSave.TabIndex = 64;
            this.btnSave.Text = "Edit User Account";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // UserPass_txt
            // 
            this.UserPass_txt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.UserPass_txt.Location = new System.Drawing.Point(195, 79);
            this.UserPass_txt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UserPass_txt.Name = "UserPass_txt";
            this.UserPass_txt.Size = new System.Drawing.Size(290, 23);
            this.UserPass_txt.TabIndex = 63;
            this.UserPass_txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UserName_txt
            // 
            this.UserName_txt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.UserName_txt.Location = new System.Drawing.Point(167, 28);
            this.UserName_txt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UserName_txt.Name = "UserName_txt";
            this.UserName_txt.ReadOnly = true;
            this.UserName_txt.Size = new System.Drawing.Size(318, 23);
            this.UserName_txt.TabIndex = 62;
            this.UserName_txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UserName_lbl
            // 
            this.UserName_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.UserName_lbl.AutoSize = true;
            this.UserName_lbl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName_lbl.Location = new System.Drawing.Point(37, 28);
            this.UserName_lbl.Name = "UserName_lbl";
            this.UserName_lbl.Size = new System.Drawing.Size(114, 24);
            this.UserName_lbl.TabIndex = 60;
            this.UserName_lbl.Text = "UserName";
            this.UserName_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Passlbel
            // 
            this.Passlbel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Passlbel.AutoSize = true;
            this.Passlbel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passlbel.Location = new System.Drawing.Point(36, 78);
            this.Passlbel.Name = "Passlbel";
            this.Passlbel.Size = new System.Drawing.Size(153, 24);
            this.Passlbel.TabIndex = 61;
            this.Passlbel.Text = "UserPassword";
            this.Passlbel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(210, 178);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(95, 28);
            this.checkBox1.TabIndex = 65;
            this.checkBox1.Text = "Active";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // EditUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 293);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.UserPass_txt);
            this.Controls.Add(this.UserName_txt);
            this.Controls.Add(this.UserName_lbl);
            this.Controls.Add(this.Passlbel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.MaximizeBox = false;
            this.Name = "EditUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit User";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox UserPass_txt;
        private System.Windows.Forms.TextBox UserName_txt;
        private System.Windows.Forms.Label UserName_lbl;
        private System.Windows.Forms.Label Passlbel;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}