﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeTecLib;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class new_user : DevExpress.XtraEditors.XtraForm
    {
        public new_user()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            var usr = ctx.UsersAccounts.FirstOrDefault(s => s.Username.ToLower().Trim().Equals(Name_txt.Text.ToLower()));


            if (usr != null)
            {
                var result = MessageBox.Show("Account already Exist !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else
            {

                if (Name_txt.Text != "" && pass_txt.Text != "")
                {
                    UsersAccounts NewUser = new UsersAccounts();
                    NewUser.Username = Name_txt.Text;
                    NewUser.UserPassword = CodeTecCodec.Base64Encode(pass_txt.Text);
                    NewUser.Active = true;

                    ctx.UsersAccounts.Add(NewUser);

                    ctx.SaveChanges();
                    var result = MessageBox.Show("Account Added Successfully !", Properties.Resources.AppName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    if (Name_txt.Text == "")
                    {

                        Name_txt.ErrorText = Resources.EmptyData;
                        Name_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    }
                    if (pass_txt.Text == "")
                    {

                        pass_txt.ErrorText = Resources.EmptyData;
                        pass_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    }
                }
              
            }
        }
    }
}