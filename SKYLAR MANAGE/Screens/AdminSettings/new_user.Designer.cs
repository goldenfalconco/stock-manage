﻿namespace SKYLAR_MANAGE.Screens.General
{
    partial class new_user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.UserName_lbl = new System.Windows.Forms.Label();
            this.Passlbel = new System.Windows.Forms.Label();
            this.Name_txt = new DevExpress.XtraEditors.TextEdit();
            this.pass_txt = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Name_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pass_txt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::SKYLAR_MANAGE.Properties.Resources.add;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(85, 193);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(341, 49);
            this.btnSave.TabIndex = 60;
            this.btnSave.Text = "Add New User";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // UserName_lbl
            // 
            this.UserName_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UserName_lbl.AutoSize = true;
            this.UserName_lbl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName_lbl.Location = new System.Drawing.Point(35, 41);
            this.UserName_lbl.Name = "UserName_lbl";
            this.UserName_lbl.Size = new System.Drawing.Size(114, 24);
            this.UserName_lbl.TabIndex = 56;
            this.UserName_lbl.Text = "UserName";
            this.UserName_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Passlbel
            // 
            this.Passlbel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Passlbel.AutoSize = true;
            this.Passlbel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passlbel.Location = new System.Drawing.Point(35, 100);
            this.Passlbel.Name = "Passlbel";
            this.Passlbel.Size = new System.Drawing.Size(159, 24);
            this.Passlbel.TabIndex = 57;
            this.Passlbel.Text = "User Password";
            this.Passlbel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Name_txt
            // 
            this.Name_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Name_txt.Location = new System.Drawing.Point(155, 39);
            this.Name_txt.Name = "Name_txt";
            this.Name_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Name_txt.Properties.Appearance.Options.UseFont = true;
            this.Name_txt.Properties.Appearance.Options.UseForeColor = true;
            this.Name_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.Name_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_txt.Size = new System.Drawing.Size(329, 28);
            this.Name_txt.TabIndex = 61;
            // 
            // pass_txt
            // 
            this.pass_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pass_txt.Location = new System.Drawing.Point(200, 98);
            this.pass_txt.Name = "pass_txt";
            this.pass_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pass_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.pass_txt.Properties.Appearance.Options.UseFont = true;
            this.pass_txt.Properties.Appearance.Options.UseForeColor = true;
            this.pass_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.pass_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pass_txt.Size = new System.Drawing.Size(284, 28);
            this.pass_txt.TabIndex = 62;
            // 
            // new_user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 293);
            this.Controls.Add(this.pass_txt);
            this.Controls.Add(this.Name_txt);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.UserName_lbl);
            this.Controls.Add(this.Passlbel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.MaximizeBox = false;
            this.Name = "new_user";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New User";
            ((System.ComponentModel.ISupportInitialize)(this.Name_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pass_txt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label UserName_lbl;
        private System.Windows.Forms.Label Passlbel;
        private DevExpress.XtraEditors.TextEdit Name_txt;
        private DevExpress.XtraEditors.TextEdit pass_txt;
    }
}