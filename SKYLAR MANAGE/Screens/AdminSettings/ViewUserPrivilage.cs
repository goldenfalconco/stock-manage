﻿using CodeTecLib;
using DevExpress.XtraEditors;

using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static DevExpress.XtraEditors.Mask.MaskSettings;
using System.Xml.Linq;
using SKYLAR_MANAGE.Properties;
using SKYLAR_MANAGE.Screens.General;

namespace SKYLAR_MANAGE.Screens.AdminSettings
{
    public partial class ViewUserPrivilage : DevExpress.XtraEditors.XtraForm
    {
        int id = 0;
        MyDbContext ctx = new MyDbContext();
        UsersAccounts User = new UsersAccounts();
        private List<int> _list;
        public ViewUserPrivilage(string userID)
        {
            InitializeComponent();
            id = int.Parse(userID);
       
         
        }

        private void ViewUserPrivilage_Load(object sender, EventArgs e)
        {
            try
            {
            
                var privileges = ctx.Privilege.ToList();



                // Load all privileges from privileges class
                foreach (var p in privileges)
                {
                    var c = new CheckBox
                    {
                        AutoSize = true,
                        Text = p.NameEn,
                        Tag = p.ID,
                        Checked = id == 1
                    };

                    if (p.ID.ToString().StartsWith("1"))
                        flwCompany.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("2"))
                        flwSuppplier.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("3"))
                        flwStock.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("4"))
                        flwConsumers.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("5"))
                        flwReports.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("6"))
                        flw6.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("7"))
                        flw7.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("8"))
                        flw8.Controls.Add(c);
                    else if (p.ID.ToString().StartsWith("9"))
                        flw9.Controls.Add(c);
                    else
                        flw6.Controls.Add(c);
                }

                // Load user
                if (id <= 0)
                {
                    chkActive.Checked = true;
                }
                else
                {


                    User = ctx.UsersAccounts.First(s => s.UserId == id);
                    txtUsername.Text = User.Username;
                    userpassword.Text = CodeTecCodec.Base64Decode(User.UserPassword);
                    if (User.Active == true)
                    {
                        chkActive.Checked = true;
                    }

                    if (id == 1)
                    {
                        chkActive.Enabled = false;
                    }

                    if (User != null)
                    {
                        User = ctx.UsersAccounts.First(s => s.UserId == id);
                        txtUsername.Text = User.Username;
                        userpassword.Text = CodeTecCodec.Base64Decode(User.UserPassword);
                       
                        chkActive.Checked = User.Active ?? false;

                        _list = !string.IsNullOrEmpty(User.Privileges)
                            ? User.Privileges.Split(',').Select(int.Parse).ToList()
                            : new List<int>();
                    }

                    if (User.UserId != 1)
                        CodeTecFunctions.SetCheckboxStateToTrue(tblAllPrivileges, _list);

                    tblAllPrivileges.Enabled = User.UserId != 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CodeTecFunctions.ChangeCheckBoxState(tblAllPrivileges, chkSelectAll.Checked);
        }

        // Add all checked check-box to a list
        private void GetSelected(Control control)
        {
            foreach (Control c in control.Controls)
                if (c is CheckBox box && box.Checked)
                {
                    var id = CodeTecNumbers.GetIntegerNumber(c.Tag);
                    _list.Add(id);
                }
                else
                {
                    if (c.HasChildren) GetSelected(c);
                }
        }

        private void Savebtn_Click(object sender, EventArgs e)
        {
            try
            {
                #region Validation

                var error = false;

                if (string.IsNullOrEmpty(txtUsername.Text))
                {
                    txtUsername.ErrorText = Resources.EmptyData;
                    txtUsername.Focus();
                    error = true;
                }

                if (string.IsNullOrEmpty(userpassword.Text))
                {
                    userpassword.ErrorText = Resources.EmptyData;
                    if (!error)
                        userpassword.Focus();
                    error = true;
                }

             

           

                if (error)
                    return;

                #endregion

                var b = ctx.UsersAccounts.FirstOrDefault(s => s.UserId == id);

                _list = new List<int>();

                GetSelected(tblAllPrivileges);

                if (id <= 0)
                {
                    User = new UsersAccounts();

                    if (b != null)
                    {
                        txtUsername.ErrorText = "Choose Another UserName";
                        txtUsername.Focus();
                        return;
                    }

                    ctx.UsersAccounts.Add(User);
                }
                else
                {
                    if (b != null)
                        if (User != null && b.UserId != User.UserId)
                        {
                            txtUsername.ErrorText = "Choose Another UserName";
                            txtUsername.Focus();
                            return;
                        }
                }

                if (User != null)
                {
                    User.Username = txtUsername.Text;
               

                    User.Privileges = id != 1 ? string.Join(",", _list) : "";
                    User.Active = chkActive.Checked;
                }

                ctx.SaveChanges();

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}