﻿using CodeTecLib;
using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Screens.AdminSettings
{
    public partial class EditUser : DevExpress.XtraEditors.XtraForm
    {

        int id = 0;
        public EditUser(string i)
        {
            InitializeComponent();
             id = int.Parse(i);
            MyDbContext ctx = new MyDbContext();
            UsersAccounts User = new UsersAccounts();
            User = ctx.UsersAccounts.First(s => s.UserId == id);

            UserName_txt.Text = User.Username;
            UserPass_txt.Text = CodeTecCodec.Base64Decode(User.UserPassword);
            if (User.Active == true)
            {
                checkBox1.Checked = true;
            }

            if (id == 1)
            {
                checkBox1.Enabled = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           
            if (UserPass_txt.Text != "")
                {
                    MyDbContext ctx = new MyDbContext();
                    UsersAccounts User = new UsersAccounts();
                    User = ctx.UsersAccounts.First(s => s.UserId == id);

                    User.UserPassword = CodeTecCodec.Base64Encode(UserPass_txt.Text);

                    User.Active = checkBox1.Checked;
                    var result = MessageBox.Show("Updated Successfully !", Properties.Resources.AppName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    ctx.SaveChanges();
                    if (result == DialogResult.OK)
                    {

                       
                        this.Close();
                    }


                }
                else
                {

                    var result = MessageBox.Show("Check Your New Password !", Properties.Resources.AppName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);

                    if (result == DialogResult.OK)
                    {


                    }
                
            }
          
        }
    }
}