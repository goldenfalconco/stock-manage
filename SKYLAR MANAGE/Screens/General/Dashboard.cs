﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Properties;
using System.Data.SqlClient;
using SKYLAR_MANAGE.Screens.Stock;
using DevExpress.Utils.CommonDialogs.Internal;
using SKYLAR_MANAGE.Screens.Suppliers;
using DialogResult = System.Windows.Forms.DialogResult;

using SKYLAR_MANAGE.Functions;
using System.ComponentModel.Design;
using SKYLAR_MANAGE.Screens.Customers;
using SKYLAR_MANAGE.ReportFotm;
using SKYLAR_MANAGE.Reports;
using DevExpress.XtraReports.UI;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class Dashboard : DevExpress.XtraEditors.XtraForm
    {
        public Dashboard()
        {
            InitializeComponent();
            this.InfoLabel.Text = Properties.Resources.AppName+" - Copyright © 2019 - " + DateTime.Now.Year.ToString() + "- Designed & Developed by "+ Properties.Resources.DevName+" | " +Settings.Default.LastUsername;
            LoadPrivileges();
        }

        private void LoadPrivileges()
        {

           
            try
            {
                var privilegee = MyFunctions.HaveAccess(MyTypes.Privilege.ViewUsers);
                admin_setting_icon.Visible = privilegee;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void exitbtn_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Exit Now ?", Properties.Resources.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {

                DialogResult dialogResult1 =
                    MessageBox.Show("System will backup Your Database Before Exit !", Properties.Resources.AppName, MessageBoxButtons.OK);
                if (dialogResult1 == DialogResult.OK)
                {



                    try
                    {
                        SqlCommand cmd = null;
                        using (var connection = new SqlConnection(ConString()))
                        {
                            connection.Open();

                            using (cmd = new SqlCommand($"BACKUP DATABASE STOCK_MANAGE_DB TO DISK = 'D:\\STOCK_MANAGE_DB.bak'; ", connection))
                            {
                                cmd.ExecuteNonQuery();
                            }
                            connection.Close();




                            DialogResult dialogResult2 = MessageBox.Show("Backup Completed !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (dialogResult2 == DialogResult.OK)
                            {

                                Application.Exit();
                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }





                }

            }
        }

        public static string ConString(bool withDb = false)
        {
            SqlConnectionStringBuilder builder;


            builder = new SqlConnectionStringBuilder
            {
                DataSource = Settings.Default.DbIP,
                IntegratedSecurity = true
            };


            return builder.ConnectionString;
        }

        private void Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {

                DialogResult dialogResult = MessageBox.Show("Exit Now ?", Properties.Resources.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    DialogResult dialogResult1 =
                        MessageBox.Show("System will backup Your Database Before Exit !", Properties.Resources.AppName, MessageBoxButtons.OK);
                    if (dialogResult1 == DialogResult.OK)
                    {



                        try
                        {
                            SqlCommand cmd = null;
                            using (var connection = new SqlConnection(ConString()))
                            {
                                connection.Open();

                                using (cmd = new SqlCommand($"BACKUP DATABASE STOCK_MANAGE_DB TO DISK = 'D:\\STOCK_MANAGE_DB.bak'; ", connection))
                                {
                                    cmd.ExecuteNonQuery();
                                }
                                connection.Close();




                                DialogResult dialogResult2 = MessageBox.Show("Backup Completed !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (dialogResult2 == DialogResult.OK)
                                {

                                    Application.Exit();
                                }


                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
                else if (dialogResult == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            ViewStock vs = new ViewStock();
            vs.Show();
        }

        private void admin_setting_icon_Click(object sender, EventArgs e)
        {
            AdminSetting w = new AdminSetting();
            w.Show();
        }

        private void supplierbtn_Click(object sender, EventArgs e)
        {
            ViewSuppliers vsu = new ViewSuppliers();
            vsu.Show();
        }

        private void settingbtn_Click(object sender, EventArgs e)
        {
            UserSetting u = new UserSetting();
            u.Show();

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            NewSupplierInvoice ninv = new NewSupplierInvoice();
            ninv.Show();
        }

        private void customerbtn_Click(object sender, EventArgs e)
        {
            ViewCustomers vsu = new ViewCustomers();
            vsu.Show();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            NewCustomerInvoice ninv = new NewCustomerInvoice();
            ninv.Show();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
           
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            var f = new SlcDate();
            f.ShowDialog();
            if (f.DialogResult != DialogResult.OK)
                return;


            var FromD = f.FromD;
            var ToD = f.ToD;

            bool hasRows = false;
            var report = new ReportSalesbyDate(FromD, ToD, ref hasRows);
            using var rpt = new ReportPrintTool(report);
            if (!hasRows)
            {

                MyInformation.Msg.ShowWarningMessage("No Data Found !");
                return;
            }

            report.CreateDocument(false);
            rpt.ShowPreviewDialog();
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            var f = new SlcDateDep();
            f.ShowDialog();
            if (f.DialogResult != DialogResult.OK)
                return;


            var FromD = f.FromD;
            var ToD = f.ToD;
            var depid = f.depId;

            bool hasRows = false;
            var report = new ReportSalebyDep(FromD, ToD,depid, ref hasRows);
            using var rpt = new ReportPrintTool(report);
            if (!hasRows)
            {

                MyInformation.Msg.ShowWarningMessage("No Data Found !");
                return;
            }

            report.CreateDocument(false);
            rpt.ShowPreviewDialog();
        }
    }
}