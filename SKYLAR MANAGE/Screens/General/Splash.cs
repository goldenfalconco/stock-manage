﻿using DevExpress.XtraSplashScreen;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using SKYLAR_MANAGE.Functions;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class Splash : SplashScreen
    {

        private enum WorkerResponse
        {
            Success,
            ErrorConfiguration,
            ErrorIpParse,
            ErrorIpUnreachable,
            ErrorIpErrorConnection,
            ErrorDbConnection
        }

        private WorkerResponse _workerResponse;
        private string _responseString;
        public Splash()
        {
            InitializeComponent();
           
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion


        private void Splash_Load(object sender, EventArgs e)
        {
            this.labelCopyright.Text = SKYLAR_MANAGE.Properties.Resources.AppName + " Copyright © 2019 - " + DateTime.Now.Year.ToString();
            this.Versionlabel.Text = " Version : " + Application.ProductVersion;
        }

        private void Splash_Shown(object sender, EventArgs e)
        {
            this.labelStatus.Text = "Loading DataBase : " + Settings.Default.DbIP;
            var update = new MyUpdate();
            update.CheckUpdate(false);
            timer1.Start();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                backgroundWorker1.ReportProgress(10);
                Thread.Sleep(100);

                // Check Configuration
                backgroundWorker1.ReportProgress(20);
                var config = MyConfigManager.ModifyConfigurationManager();
                if (!string.IsNullOrEmpty(config))
                {
                    _responseString = config;
                    _workerResponse = WorkerResponse.ErrorConfiguration;
                    backgroundWorker1.CancelAsync();
                    e.Cancel = true;
                }
                else
                {
                    backgroundWorker1.ReportProgress(30);
                    Thread.Sleep(100);
                    backgroundWorker1.ReportProgress(40);
                    Thread.Sleep(100);

                    var isLocal = Settings.Default.DbIP == @"(LocalDB)\MSSQLLocalDB";

                    // Parse IP
                    var parsed = isLocal || IPAddress.TryParse(Settings.Default.DbIP, out _);

                    if (!parsed)
                    {
                        _responseString = $@"Please Check Your Database Server IP Address";
                        _workerResponse = WorkerResponse.ErrorIpParse;
                        backgroundWorker1.CancelAsync();
                        e.Cancel = true;
                    }
                    else
                    {
                        backgroundWorker1.ReportProgress(50);
                        Thread.Sleep(100);
                        backgroundWorker1.ReportProgress(60);

                        // Ping IP
                        var error = false;

                        if (!isLocal)
                        {
                            var myPing = new Ping();
                            var reply = myPing.Send(Settings.Default.DbIP, 2000);

                            if (reply == null) // Not reachable
                            {
                                _responseString = $@"Database Server {Settings.Default.DbIP} Is Unreachable";
                                _workerResponse = WorkerResponse.ErrorIpUnreachable;
                                backgroundWorker1.CancelAsync();
                                e.Cancel = true;
                                error = true;
                            }
                            else if (reply.Status != IPStatus.Success) // no connection
                            {
                                _responseString =
                                    $@"Error Connection to Database Server {Settings.Default.DbIP} With Status: {reply.Status}";
                                _workerResponse = WorkerResponse.ErrorIpErrorConnection;
                                backgroundWorker1.CancelAsync();
                                e.Cancel = true;
                                error = true;
                            }
                        }

                        if (error)
                            return;

                        // Connected
                        Thread.Sleep(100);
                        backgroundWorker1.ReportProgress(70);

                        // Check if db is reachable
                        var connect = MyConnection.CheckConnection();
                        if (!string.IsNullOrEmpty(connect))
                        {
                            _responseString = connect;
                            _workerResponse = WorkerResponse.ErrorDbConnection;
                            backgroundWorker1.CancelAsync();
                            e.Cancel = true;
                        }
                        else
                        {
                            Thread.Sleep(100);
                            backgroundWorker1.ReportProgress(80);


                            backgroundWorker1.ReportProgress(90);
                            Thread.Sleep(100);

                            backgroundWorker1.ReportProgress(100);
                            _responseString = "Success";
                            _workerResponse = WorkerResponse.Success;

                            Thread.Sleep(100);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                /*
                 *    var result = MessageBox.Show(" Error Occurred , Check For Update ?", Resources.AppName, MessageBoxButtons.YesNo,MessageBoxIcon.Error);
                                    if (result == DialogResult.Yes)
                                   {
                                      var update = new MyUpdate();
                                     update.CheckUpdate(true);
                                    backgroundWorker1.CancelAsync();
                                 }
                                  else
                                  {
                                     if (ex.HResult == -2146233088)
                                     {
                                     backgroundWorker1.CancelAsync();
                                      MessageBox.Show(ex.ToString(), Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                  }
                                else
                                  MessageBox.Show(ex.ToString(), Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                  }
                 */

            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {
                    if (_workerResponse == WorkerResponse.Success)
                        return;
                    var result = MessageBox.Show(@"Change Database Server IP Address ? Response : " + _responseString, Resources.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                    if (result == DialogResult.Yes)
                    {
                        var f = new ConWizard();
                       f.ShowDialog();
                    
                    }
                    else
                        Close();
                }
                else if (e.Error != null)
                {
                    var result = MessageBox.Show(@"Error Occurred , Change Database Server IP Address ?", Resources.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                    if (result == DialogResult.Yes)
                    {
                        var f = new ConWizard();
                        f.ShowDialog();
                    }
                    else
                        Close();
                }
                else
                {

                    if (Settings.Default.AutoLogin == true)
                    {
                        timer1.Stop();
                        Dashboard D = new Dashboard();
                        D.Show();
                        this.Hide();
                    }
                    else
                    {
                        timer1.Stop();
                        Login l = new Login();
                        l.Show();
                        this.Hide();
                    }
                  
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            labelStatus.Text = $@"Loading {e.ProgressPercentage}%";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!MyInformation.IsCheckingUpdate)
            {
                if (!backgroundWorker1.IsBusy)
                    backgroundWorker1.RunWorkerAsync();

                timer1.Stop();
            }
        }
    }
}