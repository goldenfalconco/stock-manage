﻿namespace SKYLAR_MANAGE.Screens.General
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConnStateLabel = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.CopyRightLabel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Password_Txt = new DevExpress.XtraEditors.TextEdit();
            this.UserNameTxt = new DevExpress.XtraEditors.TextEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AutoLoginChecker = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password_Txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserNameTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ConnStateLabel
            // 
            this.ConnStateLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ConnStateLabel.AutoSize = true;
            this.ConnStateLabel.ForeColor = System.Drawing.Color.Black;
            this.ConnStateLabel.Location = new System.Drawing.Point(11, 646);
            this.ConnStateLabel.Name = "ConnStateLabel";
            this.ConnStateLabel.Size = new System.Drawing.Size(104, 16);
            this.ConnStateLabel.TabIndex = 47;
            this.ConnStateLabel.Text = "Connection State";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox3.Image = global::SKYLAR_MANAGE.Properties.Resources.data_management__2_;
            this.pictureBox3.Location = new System.Drawing.Point(383, 628);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(32, 32);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 46;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // CopyRightLabel
            // 
            this.CopyRightLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CopyRightLabel.AutoSize = true;
            this.CopyRightLabel.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CopyRightLabel.ForeColor = System.Drawing.Color.Black;
            this.CopyRightLabel.Location = new System.Drawing.Point(31, 605);
            this.CopyRightLabel.Name = "CopyRightLabel";
            this.CopyRightLabel.Size = new System.Drawing.Size(0, 16);
            this.CopyRightLabel.TabIndex = 45;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::SKYLAR_MANAGE.Properties.Resources.enter__1_;
            this.pictureBox2.Location = new System.Drawing.Point(175, 473);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 44;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // Password_Txt
            // 
            this.Password_Txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Password_Txt.Location = new System.Drawing.Point(114, 336);
            this.Password_Txt.Margin = new System.Windows.Forms.Padding(4);
            this.Password_Txt.Name = "Password_Txt";
            this.Password_Txt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.Password_Txt.Properties.Appearance.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password_Txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Password_Txt.Properties.Appearance.Options.UseBackColor = true;
            this.Password_Txt.Properties.Appearance.Options.UseFont = true;
            this.Password_Txt.Properties.Appearance.Options.UseForeColor = true;
            this.Password_Txt.Properties.Appearance.Options.UseTextOptions = true;
            this.Password_Txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Password_Txt.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Password_Txt.Properties.PasswordChar = '*';
            this.Password_Txt.Size = new System.Drawing.Size(210, 30);
            this.Password_Txt.TabIndex = 43;
            this.Password_Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Password_Txt_KeyDown);
            // 
            // UserNameTxt
            // 
            this.UserNameTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UserNameTxt.EditValue = "admin";
            this.UserNameTxt.Location = new System.Drawing.Point(114, 278);
            this.UserNameTxt.Margin = new System.Windows.Forms.Padding(4);
            this.UserNameTxt.Name = "UserNameTxt";
            this.UserNameTxt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.UserNameTxt.Properties.Appearance.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameTxt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.UserNameTxt.Properties.Appearance.Options.UseBackColor = true;
            this.UserNameTxt.Properties.Appearance.Options.UseFont = true;
            this.UserNameTxt.Properties.Appearance.Options.UseForeColor = true;
            this.UserNameTxt.Properties.Appearance.Options.UseTextOptions = true;
            this.UserNameTxt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UserNameTxt.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.UserNameTxt.Size = new System.Drawing.Size(210, 30);
            this.UserNameTxt.TabIndex = 42;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(15, 373);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 1);
            this.panel2.TabIndex = 41;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(14, 315);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 1);
            this.panel1.TabIndex = 40;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(15, 348);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(73, 18);
            this.labelControl2.TabIndex = 39;
            this.labelControl2.Text = "Password";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(15, 290);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(75, 18);
            this.labelControl1.TabIndex = 38;
            this.labelControl1.Text = "Username";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.pictureBox1.Location = new System.Drawing.Point(150, 27);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // AutoLoginChecker
            // 
            this.AutoLoginChecker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AutoLoginChecker.AutoSize = true;
            this.AutoLoginChecker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.AutoLoginChecker.ForeColor = System.Drawing.Color.White;
            this.AutoLoginChecker.Location = new System.Drawing.Point(325, 395);
            this.AutoLoginChecker.Name = "AutoLoginChecker";
            this.AutoLoginChecker.Size = new System.Drawing.Size(89, 20);
            this.AutoLoginChecker.TabIndex = 49;
            this.AutoLoginChecker.Text = "Auto Login";
            this.AutoLoginChecker.UseVisualStyleBackColor = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 671);
            this.Controls.Add(this.AutoLoginChecker);
            this.Controls.Add(this.ConnStateLabel);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.CopyRightLabel);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.Password_Txt);
            this.Controls.Add(this.UserNameTxt);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureBox1);
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_FormClosing);
            this.Shown += new System.EventHandler(this.Login_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Password_Txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserNameTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ConnStateLabel;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label CopyRightLabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.TextEdit Password_Txt;
        private DevExpress.XtraEditors.TextEdit UserNameTxt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox AutoLoginChecker;
    }
}