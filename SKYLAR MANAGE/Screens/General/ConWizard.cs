﻿using CodeTecLib;
using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class ConWizard : DevExpress.XtraEditors.XtraForm
    {
        public ConWizard()
        {
            InitializeComponent();
        }

        private void ConWizard_Load(object sender, EventArgs e)
        {
            txtIp.Text = Settings.Default.DbIP;
            Dbtxt.Text = Settings.Default.DbName;
            Usertxt.Text = Settings.Default.DbUser;
            Passtxt.Text = Settings.Default.DbPass;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Settings.Default.DbIP = txtIp.Text;
                Settings.Default.DbName = Dbtxt.Text;
                Settings.Default.DbUser = CodeTecCodec.Base64Encode(Usertxt.Text);
                Settings.Default.DbPass = CodeTecCodec.Base64Encode(Passtxt.Text);



                Settings.Default.Save();


                DialogResult dialogResult = MessageBox.Show("System will Restart to Apply change's !", Properties.Resources.AppName, MessageBoxButtons.OK);
                if (dialogResult == DialogResult.OK)
                {

                    Application.Restart();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), Properties.Resources.AppName, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtIp.Text == "(LocalDB)\\MSSQLLocalDB" && Passtxt.Text == "" && Usertxt.Text == "")
            {
                try
                {

                    DialogResult dialogResult1 =
                     MessageBox.Show("Initialize Database ?", Properties.Resources.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult1 == DialogResult.Yes)
                    {
                        var output = "";
                        var input = MyFunctions.InputBox(Application.ProductName, "Password: ", ref output, true);

                        if (input == DialogResult.OK)
                        {
                            if (output.Equals($@"{DateTime.Now.Day}"))
                            {

                                try
                                {

                                    var cmdText = "select count(*) from master.dbo.sysdatabases where name=@database";
                                    using (var connection = new SqlConnection(ConString()))
                                    {
                                        using (var sqlCmd = new SqlCommand(cmdText, connection))
                                        {
                                            // Use parameters to protect against Sql Injection
                                            sqlCmd.Parameters.Add("@database", System.Data.SqlDbType.NVarChar).Value = Settings.Default.DbName;

                                            // Open the connection as late as possible
                                            connection.Open();


                                            //not exist 
                                            if (Convert.ToInt32(sqlCmd.ExecuteScalar()) == 0)
                                            {
                                                try
                                                {
                                                    SqlCommand cmd = null;
                                                    using (var connection2 = new SqlConnection(ConString()))
                                                    {
                                                        connection2.Open();

                                                        using (cmd = new SqlCommand($"If(db_id(N'{Settings.Default.DbName}') IS NULL) CREATE DATABASE [{Settings.Default.DbName}]", connection2))
                                                        {
                                                            cmd.ExecuteNonQuery();
                                                        }
                                                        connection2.Close();


                                                        var result = MessageBox.Show("Database Initialization Successfully !", Properties.Resources.AppName,
                                                            MessageBoxButtons.OK,
                                                            MessageBoxIcon.Information);

                                                        Settings.Default.DbIP = txtIp.Text;
                                                        Settings.Default.DbName = Dbtxt.Text;

                                                        Settings.Default.DbUser = CodeTecCodec.Base64Encode(Usertxt.Text);
                                                        Settings.Default.DbPass = CodeTecCodec.Base64Encode(Passtxt.Text);
                                                        Settings.Default.Save();


                                                        if (result == DialogResult.OK)
                                                        {


                                                            DialogResult dialogResult = MessageBox.Show("System will Restart to Apply change's !", Properties.Resources.AppName, MessageBoxButtons.OK);
                                                            if (dialogResult == DialogResult.OK)
                                                            {

                                                                Application.Restart();
                                                            }
                                                        }

                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    MessageBox.Show(ex.Message);
                                                }



                                            }
                                            else
                                            {
                                                DialogResult result = MessageBox.Show("Database Already Initialized !", Properties.Resources.AppName,
                                                    MessageBoxButtons.OK,
                                                    MessageBoxIcon.Error);

                                                if (result == DialogResult.OK)
                                                {
                                                    Settings.Default.DbIP = txtIp.Text;
                                                    Settings.Default.DbName = Dbtxt.Text;

                                                    Settings.Default.DbUser = CodeTecCodec.Base64Encode(Usertxt.Text);
                                                    Settings.Default.DbPass = CodeTecCodec.Base64Encode(Passtxt.Text);
                                                    Settings.Default.Save();



                                                    DialogResult dialogResult = MessageBox.Show("System will Restart to Apply change's !", Properties.Resources.AppName, MessageBoxButtons.OK);
                                                    if (dialogResult == DialogResult.OK)
                                                    {

                                                        Application.Restart();
                                                    }
                                                }

                                            }

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message.ToString());
                                }



                            }
                        }
                    }



                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), Properties.Resources.AppName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("System can Initialize Database Only on Local Servers", SKYLAR_MANAGE.Properties.Resources.AppName,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCheckIp_Click(object sender, EventArgs e)
        {
            try
            {
                var myPing = new Ping();
                var reply = myPing.Send(txtIp.Text, 2000);

                if (reply == null)
                {

                    MessageBox.Show($@"The Database Server {txtIp.Text} is unreachable !", Resources.AppName, MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                if (reply.Status != IPStatus.Success)
                {

                    MessageBox.Show($@"Error Connection to Database Server {txtIp.Text}
State: {reply.Status} !", Resources.AppName, MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }


                MessageBox.Show($@"{txtIp.Text}  is Successfully Connected !", Resources.AppName, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, Resources.AppName, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public static string ConString(bool withDb = false)
        {
            SqlConnectionStringBuilder builder;


            builder = new SqlConnectionStringBuilder
            {
                DataSource = Settings.Default.DbIP,
                IntegratedSecurity = true
            };


            return builder.ConnectionString;
        }
    }
}