﻿using CodeTecLib;
using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class UserSetting : DevExpress.XtraEditors.XtraForm
    {
        public UserSetting()
        {
            InitializeComponent();
            UserName_txt.Text = Settings.Default.LastUsername;
            AutoLoginChecker.Checked = Settings.Default.AutoLogin;
            Settings.Default.Save();
        }

        private void UserSetting_Shown(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            UsersAccounts LoginUser = new UsersAccounts();
            LoginUser = ctx.UsersAccounts.First(s => s.UserId == Settings.Default.LastUserId);

            UserPass_txt.Text = CodeTecCodec.Base64Decode(LoginUser.UserPassword);
        
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (UserPass_txt.Text != "")
            {
                MyDbContext ctx = new MyDbContext();
                UsersAccounts LoginUser = new UsersAccounts();
                LoginUser = ctx.UsersAccounts.First(s => s.UserId == Settings.Default.LastUserId);

                LoginUser.UserPassword = CodeTecCodec.Base64Encode(UserPass_txt.Text);
              

                Settings.Default.AutoLogin = AutoLoginChecker.Checked;
                Settings.Default.Save();


                var result = MessageBox.Show("Updated Successfully !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                if (result == DialogResult.OK)
                {

                    ctx.SaveChanges();
                    DialogResult dialogResult = MessageBox.Show("System will Restart to Apply change !", Properties.Resources.AppName, MessageBoxButtons.OK);
                    if (dialogResult == DialogResult.OK)
                    {

                        Application.Restart();
                    }
                }


            }
            else
            {

                var result = MessageBox.Show("Check Your New Password !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                if (result == DialogResult.OK)
                {

                   
                }
            }

        }
    }
}