﻿namespace SKYLAR_MANAGE.Screens.General
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.exitbtn = new DevExpress.XtraEditors.SimpleButton();
            this.settingbtn = new DevExpress.XtraEditors.SimpleButton();
            this.supplierbtn = new DevExpress.XtraEditors.SimpleButton();
            this.customerbtn = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.admin_setting_icon = new System.Windows.Forms.PictureBox();
            this.Backup_label = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.admin_setting_icon)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.simpleButton4);
            this.pnlMain.Controls.Add(this.simpleButton5);
            this.pnlMain.Controls.Add(this.simpleButton3);
            this.pnlMain.Controls.Add(this.simpleButton2);
            this.pnlMain.Controls.Add(this.simpleButton1);
            this.pnlMain.Controls.Add(this.InfoLabel);
            this.pnlMain.Controls.Add(this.exitbtn);
            this.pnlMain.Controls.Add(this.settingbtn);
            this.pnlMain.Controls.Add(this.supplierbtn);
            this.pnlMain.Controls.Add(this.customerbtn);
            this.pnlMain.Location = new System.Drawing.Point(41, 6);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1305, 571);
            this.pnlMain.TabIndex = 6;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton4.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.report__2_;
            this.simpleButton4.Location = new System.Drawing.Point(516, 431);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(279, 86);
            this.simpleButton4.TabIndex = 16;
            this.simpleButton4.Text = "Dep.Sales Report";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton5.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.bill__2_;
            this.simpleButton5.Location = new System.Drawing.Point(516, 267);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(279, 134);
            this.simpleButton5.TabIndex = 15;
            this.simpleButton5.Text = "Sales Report";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton3.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.empty_cart;
            this.simpleButton3.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton3.Location = new System.Drawing.Point(854, 267);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(375, 134);
            this.simpleButton3.TabIndex = 13;
            this.simpleButton3.Text = "Customer Invoice";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton2.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.warehouse;
            this.simpleButton2.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton2.Location = new System.Drawing.Point(516, 58);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(279, 185);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "View Stock";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton1.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.shopping_cart;
            this.simpleButton1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton1.Location = new System.Drawing.Point(77, 172);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(375, 134);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "Supplier Invoice";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // InfoLabel
            // 
            this.InfoLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.InfoLabel.AutoSize = true;
            this.InfoLabel.ForeColor = System.Drawing.Color.Black;
            this.InfoLabel.Location = new System.Drawing.Point(4, 551);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(35, 13);
            this.InfoLabel.TabIndex = 10;
            this.InfoLabel.Text = "label1";
            // 
            // exitbtn
            // 
            this.exitbtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.exitbtn.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.exitbtn.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitbtn.Appearance.Options.UseBackColor = true;
            this.exitbtn.Appearance.Options.UseFont = true;
            this.exitbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitbtn.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.power;
            this.exitbtn.Location = new System.Drawing.Point(854, 431);
            this.exitbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitbtn.Name = "exitbtn";
            this.exitbtn.Size = new System.Drawing.Size(375, 86);
            this.exitbtn.TabIndex = 8;
            this.exitbtn.Text = "Exit";
            this.exitbtn.Click += new System.EventHandler(this.exitbtn_Click);
            // 
            // settingbtn
            // 
            this.settingbtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.settingbtn.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.settingbtn.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingbtn.Appearance.Options.UseBackColor = true;
            this.settingbtn.Appearance.Options.UseFont = true;
            this.settingbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.settingbtn.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.cloud_computing;
            this.settingbtn.Location = new System.Drawing.Point(77, 58);
            this.settingbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.settingbtn.Name = "settingbtn";
            this.settingbtn.Size = new System.Drawing.Size(375, 86);
            this.settingbtn.TabIndex = 7;
            this.settingbtn.Text = "Settings";
            this.settingbtn.Click += new System.EventHandler(this.settingbtn_Click);
            // 
            // supplierbtn
            // 
            this.supplierbtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.supplierbtn.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.supplierbtn.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierbtn.Appearance.Options.UseBackColor = true;
            this.supplierbtn.Appearance.Options.UseFont = true;
            this.supplierbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.supplierbtn.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.supplier__3_;
            this.supplierbtn.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.supplierbtn.Location = new System.Drawing.Point(77, 332);
            this.supplierbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.supplierbtn.Name = "supplierbtn";
            this.supplierbtn.Size = new System.Drawing.Size(375, 185);
            this.supplierbtn.TabIndex = 3;
            this.supplierbtn.Text = "View Suppliers";
            this.supplierbtn.Click += new System.EventHandler(this.supplierbtn_Click);
            // 
            // customerbtn
            // 
            this.customerbtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.customerbtn.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.customerbtn.Appearance.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerbtn.Appearance.Options.UseBackColor = true;
            this.customerbtn.Appearance.Options.UseFont = true;
            this.customerbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customerbtn.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.customer__1_;
            this.customerbtn.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.customerbtn.Location = new System.Drawing.Point(862, 58);
            this.customerbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.customerbtn.Name = "customerbtn";
            this.customerbtn.Size = new System.Drawing.Size(375, 185);
            this.customerbtn.TabIndex = 1;
            this.customerbtn.Text = "View Customers";
            this.customerbtn.Click += new System.EventHandler(this.customerbtn_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.admin_setting_icon);
            this.panel1.Controls.Add(this.Backup_label);
            this.panel1.Location = new System.Drawing.Point(41, 583);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1305, 27);
            this.panel1.TabIndex = 7;
            // 
            // admin_setting_icon
            // 
            this.admin_setting_icon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.admin_setting_icon.Image = global::SKYLAR_MANAGE.Properties.Resources.settings__2_;
            this.admin_setting_icon.Location = new System.Drawing.Point(4, 4);
            this.admin_setting_icon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.admin_setting_icon.Name = "admin_setting_icon";
            this.admin_setting_icon.Size = new System.Drawing.Size(16, 16);
            this.admin_setting_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.admin_setting_icon.TabIndex = 13;
            this.admin_setting_icon.TabStop = false;
            this.admin_setting_icon.Click += new System.EventHandler(this.admin_setting_icon_Click);
            // 
            // Backup_label
            // 
            this.Backup_label.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Backup_label.AutoSize = true;
            this.Backup_label.ForeColor = System.Drawing.Color.Black;
            this.Backup_label.Location = new System.Drawing.Point(-61, 4);
            this.Backup_label.Name = "Backup_label";
            this.Backup_label.Size = new System.Drawing.Size(0, 13);
            this.Backup_label.TabIndex = 12;
            this.Backup_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1388, 613);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GFCO STOCK MANAGE - Dashboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Dashboard_FormClosing);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.admin_setting_icon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlMain;
        private DevExpress.XtraEditors.SimpleButton exitbtn;
        private System.Windows.Forms.Label InfoLabel;
        private DevExpress.XtraEditors.SimpleButton settingbtn;
        private DevExpress.XtraEditors.SimpleButton supplierbtn;
        private DevExpress.XtraEditors.SimpleButton customerbtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Backup_label;
        private System.Windows.Forms.PictureBox admin_setting_icon;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
    }
}