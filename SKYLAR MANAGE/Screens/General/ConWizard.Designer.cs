﻿namespace SKYLAR_MANAGE.Screens.General
{
    partial class ConWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.Passtxt = new System.Windows.Forms.TextBox();
            this.Usertxt = new System.Windows.Forms.TextBox();
            this.Dbtxt = new System.Windows.Forms.TextBox();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.lblIp = new System.Windows.Forms.Label();
            this.lblDb = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.btnCheckIp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.button2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(29, 347);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(452, 49);
            this.button2.TabIndex = 51;
            this.button2.Text = "Initialize DataBase";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(88, 282);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(336, 47);
            this.btnSave.TabIndex = 48;
            this.btnSave.Text = "Save Setting\'s";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Passtxt
            // 
            this.Passtxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Passtxt.Location = new System.Drawing.Point(271, 202);
            this.Passtxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Passtxt.Name = "Passtxt";
            this.Passtxt.Size = new System.Drawing.Size(210, 23);
            this.Passtxt.TabIndex = 46;
            this.Passtxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Usertxt
            // 
            this.Usertxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Usertxt.Location = new System.Drawing.Point(271, 162);
            this.Usertxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Usertxt.Name = "Usertxt";
            this.Usertxt.Size = new System.Drawing.Size(210, 23);
            this.Usertxt.TabIndex = 45;
            this.Usertxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Dbtxt
            // 
            this.Dbtxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Dbtxt.Location = new System.Drawing.Point(207, 122);
            this.Dbtxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Dbtxt.Name = "Dbtxt";
            this.Dbtxt.Size = new System.Drawing.Size(274, 23);
            this.Dbtxt.TabIndex = 44;
            this.Dbtxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIp
            // 
            this.txtIp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtIp.Location = new System.Drawing.Point(158, 82);
            this.txtIp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(219, 23);
            this.txtIp.TabIndex = 43;
            this.txtIp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblIp
            // 
            this.lblIp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblIp.AutoSize = true;
            this.lblIp.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIp.Location = new System.Drawing.Point(32, 81);
            this.lblIp.Name = "lblIp";
            this.lblIp.Size = new System.Drawing.Size(120, 24);
            this.lblIp.TabIndex = 39;
            this.lblIp.Text = "IP Address";
            this.lblIp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDb
            // 
            this.lblDb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDb.Location = new System.Drawing.Point(32, 121);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(169, 24);
            this.lblDb.TabIndex = 40;
            this.lblDb.Text = "DataBase Name";
            this.lblDb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUser
            // 
            this.lblUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(32, 161);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(233, 24);
            this.lblUser.TabIndex = 41;
            this.lblUser.Text = "Connection UserName";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPass
            // 
            this.lblPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPass.AutoSize = true;
            this.lblPass.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPass.Location = new System.Drawing.Point(32, 201);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(226, 24);
            this.lblPass.TabIndex = 42;
            this.lblPass.Text = "Connection Password";
            this.lblPass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCheckIp
            // 
            this.btnCheckIp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCheckIp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnCheckIp.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckIp.ForeColor = System.Drawing.Color.Yellow;
            this.btnCheckIp.Location = new System.Drawing.Point(385, 73);
            this.btnCheckIp.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnCheckIp.Name = "btnCheckIp";
            this.btnCheckIp.Size = new System.Drawing.Size(96, 39);
            this.btnCheckIp.TabIndex = 53;
            this.btnCheckIp.Text = "Check IP";
            this.btnCheckIp.UseVisualStyleBackColor = false;
            this.btnCheckIp.Click += new System.EventHandler(this.btnCheckIp_Click);
            // 
            // ConWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 453);
            this.Controls.Add(this.btnCheckIp);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.Passtxt);
            this.Controls.Add(this.Usertxt);
            this.Controls.Add(this.Dbtxt);
            this.Controls.Add(this.txtIp);
            this.Controls.Add(this.lblIp);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblPass);
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.MaximizeBox = false;
            this.Name = "ConWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wizard";
            this.Load += new System.EventHandler(this.ConWizard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox Passtxt;
        private System.Windows.Forms.TextBox Usertxt;
        private System.Windows.Forms.TextBox Dbtxt;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Label lblIp;
        private System.Windows.Forms.Label lblDb;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Button btnCheckIp;
    }
}