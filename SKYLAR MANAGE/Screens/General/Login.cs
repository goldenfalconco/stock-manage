﻿using CodeTecLib;
using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;

namespace SKYLAR_MANAGE.Screens.General
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        MyDbContext ctx = new MyDbContext();
        public Login()
        {
            InitializeComponent();
            this.CopyRightLabel.Text =
                Properties.Resources.AppName + " Copyright © 2019 - " + DateTime.Now.Year.ToString();
            if (Settings.Default.LastUsername == "")
            {

                this.ActiveControl = UserNameTxt;
            }
            else
            {
                UserNameTxt.Text = Settings.Default.LastUsername;
                this.ActiveControl = Password_Txt;
            }

            AutoLoginChecker.Checked = Settings.Default.AutoLogin;


        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Application.Exit();
            }
        }

        private void Login_Shown(object sender, EventArgs e)
        {
           
            var connect = MyConnection.CheckConnection();

            if (string.IsNullOrEmpty(connect))
            {

                ConnStateLabel.Text = "Database Server " + Settings.Default.DbIP + " Connected !";
            }
            else
            {
                ConnStateLabel.Text = "Cannot Connect To Server : " + Settings.Default.DbIP;
            }
        }

        private void Password_Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pictureBox2_Click(sender, e);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            MyConfigManager.ModifyConfigurationManager();


            var connect = MyConnection.CheckConnection();
            if (!string.IsNullOrEmpty(connect))
                return;
            var password = Password_Txt.Text.Trim();
            password = CodeTecCodec.Base64Encode(password);
            
            var usr = ctx.UsersAccounts.FirstOrDefault(s => s.Username.ToLower().Trim().Equals(UserNameTxt.Text.ToLower()) && s.UserPassword.Trim().Equals(password) && s.Active==true);
            if (usr != null)
            {


                var mac = MyFunctions.GetMacAddress();
                if (mac != null && !string.IsNullOrEmpty(mac))
                {
                    var PCInfo = ctx.UsersPcInfos.FirstOrDefault(s => s.MacAddress.Equals(mac));

                    if (PCInfo == null)
                    {
                        PCInfo = new UsersPcInfo()
                        {
                            PcIp = MyFunctions.GetIpAddress(),
                            PcName = MyFunctions.GetName(),
                            WindowsVersion = MyFunctions.GetOsFriendlyName(),
                            MacAddress = mac,
                            AppVersion = Application.ProductVersion,
                            LastLogin = DateTime.Now
                        };

                        ctx.UsersPcInfos.Add(PCInfo);
                    }
                    else

                    {
                        PCInfo.PcIp = MyFunctions.GetIpAddress();
                        PCInfo.PcName = MyFunctions.GetName();
                        PCInfo.WindowsVersion = MyFunctions.GetOsFriendlyName();
                        PCInfo.MacAddress = mac;
                        PCInfo.AppVersion = Application.ProductVersion;
                        PCInfo.LastLogin = DateTime.Now;
                    }

                    ctx.SaveChanges();


                    Settings.Default.LastUserId = usr.UserId;
                    Settings.Default.LastUsername = UserNameTxt.Text;

                    Settings.Default.Save();
                }

                if (AutoLoginChecker.Checked == true)
                {
                    Settings.Default.AutoLogin = true;
                    Settings.Default.Save();
                }
              Dashboard D = new Dashboard(); 
              D.Show();
                Hide();

            }
            else
            {


                UserNameTxt.ErrorText = Resources.InvalidAccount;
                UserNameTxt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;

                UserNameTxt.Text = "";
                Password_Txt.Text = "";

                UserNameTxt.Focus();


            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ConWizard cw = new ConWizard();
            cw.Show();
        }
    }
}