﻿namespace SKYLAR_MANAGE.Screens.Suppliers
{
    partial class AddSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.UserName_lbl = new System.Windows.Forms.Label();
            this.Passlbel = new System.Windows.Forms.Label();
            this.Name_txt = new DevExpress.XtraEditors.TextEdit();
            this.Phone_txt = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Name_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone_txt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::SKYLAR_MANAGE.Properties.Resources.add;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(80, 193);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(341, 49);
            this.btnSave.TabIndex = 55;
            this.btnSave.Text = "Add New Supplier";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // UserName_lbl
            // 
            this.UserName_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UserName_lbl.AutoSize = true;
            this.UserName_lbl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName_lbl.Location = new System.Drawing.Point(36, 48);
            this.UserName_lbl.Name = "UserName_lbl";
            this.UserName_lbl.Size = new System.Drawing.Size(159, 24);
            this.UserName_lbl.TabIndex = 51;
            this.UserName_lbl.Text = "Supplier Name";
            this.UserName_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Passlbel
            // 
            this.Passlbel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Passlbel.AutoSize = true;
            this.Passlbel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passlbel.Location = new System.Drawing.Point(36, 106);
            this.Passlbel.Name = "Passlbel";
            this.Passlbel.Size = new System.Drawing.Size(160, 24);
            this.Passlbel.TabIndex = 52;
            this.Passlbel.Text = "Phone Number";
            this.Passlbel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Name_txt
            // 
            this.Name_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Name_txt.Location = new System.Drawing.Point(202, 46);
            this.Name_txt.Name = "Name_txt";
            this.Name_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Name_txt.Properties.Appearance.Options.UseFont = true;
            this.Name_txt.Properties.Appearance.Options.UseForeColor = true;
            this.Name_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.Name_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_txt.Size = new System.Drawing.Size(283, 28);
            this.Name_txt.TabIndex = 56;
            // 
            // Phone_txt
            // 
            this.Phone_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Phone_txt.Location = new System.Drawing.Point(202, 106);
            this.Phone_txt.Name = "Phone_txt";
            this.Phone_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Phone_txt.Properties.Appearance.Options.UseFont = true;
            this.Phone_txt.Properties.Appearance.Options.UseForeColor = true;
            this.Phone_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.Phone_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Phone_txt.Size = new System.Drawing.Size(283, 28);
            this.Phone_txt.TabIndex = 57;
            this.Phone_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Phone_txt_KeyPress);
            // 
            // AddSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 293);
            this.Controls.Add(this.Phone_txt);
            this.Controls.Add(this.Name_txt);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.UserName_lbl);
            this.Controls.Add(this.Passlbel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.MaximizeBox = false;
            this.Name = "AddSupplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Supplier";
            ((System.ComponentModel.ISupportInitialize)(this.Name_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Phone_txt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label UserName_lbl;
        private System.Windows.Forms.Label Passlbel;
        private DevExpress.XtraEditors.TextEdit Name_txt;
        private DevExpress.XtraEditors.TextEdit Phone_txt;
    }
}