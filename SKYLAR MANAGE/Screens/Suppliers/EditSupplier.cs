﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Models;

namespace SKYLAR_MANAGE.Screens.Suppliers
{
    public partial class EditSupplier : DevExpress.XtraEditors.XtraForm
    {
        MyDbContext ctx = new MyDbContext();
        SuppliersAccount suppliersAccount = new SuppliersAccount();

        public EditSupplier(int SupplierId)
        {
            InitializeComponent();


            suppliersAccount = ctx.SuppliersAccounts.Find(SupplierId);
            Name_txt.Text = suppliersAccount.SupplierName;
            Phone_txt.Text = suppliersAccount.SupplierPhone;
          
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            suppliersAccount.SupplierName = Name_txt.Text;
            suppliersAccount.SupplierPhone = Phone_txt.Text;
            ctx.SaveChanges();
            var result = MessageBox.Show("Updated Successfully !", Properties.Resources.AppName,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            if (result == DialogResult.OK)
            {

                this.Close();
            }
        }

        private void Phone_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            //only Numbers
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}