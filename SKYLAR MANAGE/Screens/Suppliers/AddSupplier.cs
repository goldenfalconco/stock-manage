﻿using CodeTecLib;
using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Screens.Suppliers
{
    public partial class AddSupplier : DevExpress.XtraEditors.XtraForm
    {
        public AddSupplier()
        {
            InitializeComponent();
        }

        
        private void btnSave_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            var usr = ctx.SuppliersAccounts.FirstOrDefault(s => s.SupplierName.ToLower().Trim().Equals(Name_txt.Text.ToLower()));


            if (usr != null)
            {
                var result = MessageBox.Show("Account already Exist !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else
            {

                if (Name_txt.Text != "")
                {
                    SuppliersAccount NewSupplier = new SuppliersAccount();
                    NewSupplier.SupplierName = Name_txt.Text;
                    NewSupplier.SupplierPhone = Phone_txt.Text;

                    ctx.SuppliersAccounts.Add(NewSupplier);

                    ctx.SaveChanges();
                    var result = MessageBox.Show("Account Added Successfully !", Properties.Resources.AppName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    if (Name_txt.Text == "")
                    {

                        Name_txt.ErrorText = Resources.EmptyData;
                        Name_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    }
                }
              
            }
        }

        private void Phone_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            //only Numbers
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}