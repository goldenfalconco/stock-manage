﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Functions;

namespace SKYLAR_MANAGE.Screens.Suppliers
{
    public partial class SupplierViewInvoice : DevExpress.XtraEditors.XtraForm
    {
        private int _SupplierId;
        public SupplierViewInvoice(int supplierid)
        {
            InitializeComponent();
            _SupplierId = supplierid;
            MyDbContext ctx = new MyDbContext();
            gridView1.ApplyGridStyle("Invoices", "Invoices", true, append: true,
              allowDelete: false);
            SupplierTransactionBinding.DataSource = ctx.SupplierTransactions.Where(s=>s.SupplierId==_SupplierId).ToList();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.SupplierTransaction suppliertran)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "NewSupplierInvoice")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    NewSupplierInvoice vs = new NewSupplierInvoice(suppliertran.AutoNbr);
                    vs.Show();
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}