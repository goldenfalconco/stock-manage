﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Screens.Stock;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DashboardCommon;
using DevExpress.XtraGrid.Views.Grid;
using SKYLAR_MANAGE.Functions;

namespace SKYLAR_MANAGE.Screens.Suppliers
{
    public partial class ViewSuppliers : DevExpress.XtraEditors.XtraForm
    { 
       
        public ViewSuppliers()
        {
            InitializeComponent();
            MyDbContext ctx = new MyDbContext();
            gridView1.ApplyGridStyle("Suppliers", "All Suppliers", true, append: true,
              allowDelete: false);
            suppliersAccountBindingSource.DataSource = ctx.SuppliersAccounts.ToList();
        }

    
        private void refreshbtn_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            suppliersAccountBindingSource.DataSource = ctx.SuppliersAccounts.ToList();
            
        }

       

        private void Editbtn_Click(object sender, EventArgs e)
        {
            if (gridView1.GetFocusedRow() is not Models.SuppliersAccount supplier)
                return;
            
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "EditSupplier")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    EditSupplier ai = new EditSupplier(supplier.SupplierId);
                    ai.ShowDialog();
                    refreshbtn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            
          
          
        }

        private void addbtn_Click(object sender, EventArgs e)
        {

            Boolean frmopen = false;
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                //iterate through
                if (frm.Name == "AddSupplier")
                {
                    frmopen = true;
                }

            }

            if (!frmopen)
            {
                AddSupplier ai = new AddSupplier();
                ai.ShowDialog();
                refreshbtn_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void ViewSuppliers_FormClosing(object sender, FormClosingEventArgs e)
        {
          
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

            try
            {
                if (gridView1.GetFocusedRow() is not Models.SuppliersAccount supplier)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "EditSupplier")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    EditSupplier ai = new EditSupplier(supplier.SupplierId);
                    ai.ShowDialog();
                    refreshbtn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.SuppliersAccount supplier)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "SupplierInvoice")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    SupplierViewInvoice vs = new SupplierViewInvoice(supplier.SupplierId);
                    vs.Show();
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
           
        }
    } 
}