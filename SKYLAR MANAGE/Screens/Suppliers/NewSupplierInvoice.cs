﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;

using System.Data.Entity;
using SKYLAR_MANAGE.Properties;
using SKYLAR_MANAGE.Screens.Stock;
using static SKYLAR_MANAGE.Functions.MyTypes;
using System.Data.Entity.Validation;
using System.Drawing.Text;
using DevExpress.Data.Helpers;
using DevExpress.Printing.Core.PdfExport.Metafile;
using DevExpress.XtraGrid.Views.Grid;
using SKYLAR_MANAGE.Reports;
using static System.Data.Entity.Infrastructure.Design.Executor;
using DevExpress.XtraReports.UI;

namespace SKYLAR_MANAGE.Screens.Suppliers
{
    public partial class NewSupplierInvoice : DevExpress.XtraEditors.XtraForm
    {


        private int _invoiceId;

        private MyDbContext _ctx;
        private SupplierTransaction _Invoice;
        private List<SupplierTransactionItem> ItemsList;
        private int _transactionType;

        public NewSupplierInvoice(int invoiceid=0)
        {
            InitializeComponent(); 
            _ctx = new MyDbContext();
            _invoiceId = invoiceid;
            
         
        }

        public int LoadBindings(bool isRefresh = false)
        {
            try
            {
                 var ctx = new MyDbContext();
                AccSearchLookUpEdit.MyInitialize(MyExtensions.Type.Supplier, ctx, isRefresh, LoadBindings, showAddButton: true);
                TransactionTypeIdTextEdit.MyInitialize(MyExtensions.Type.Transaction, ctx, isRefresh, LoadBindings, showAddButton: false);
                repositoryItemSearchLookUpItems.MyInitialize2(MyExtensions.Type.stock, ctx, isRefresh, LoadBindings, showAddButton: true);
                gvwItems.ApplyGridStyle("FrmSupplierInvoice", "Items", true, append: true,
                    allowDelete: true);
                gvwItems.AddNewRow();
                return 1;
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);

                return 0;
            }
        }
     
        private void NewInvoice_Load(object sender, EventArgs e)
        {

            if (_invoiceId == 0)
            {
                BdgItems.DataSource = _ctx.SupplierTransactionsItem.Local.ToBindingList();
                DateDateEdit.DateTime = DateTime.Today;
            }
            //edit
            else
            {
                _ctx.SupplierTransactionsItem.Where(v => v.Invoice.AutoNbr == _invoiceId).Load();
                BdgItems.DataSource = _ctx.SupplierTransactionsItem.Local.ToBindingList();
                _Invoice = _ctx.SupplierTransactions.Find(_invoiceId);
                if (_Invoice != null)
                {
                    DateDateEdit.DateTime = _Invoice.Date;
                    AutoNbTextEdit.Text = _Invoice.AutoNbr.ToString();
                    NotesTextEdit.Text = _Invoice.Notes;
                    AccSearchLookUpEdit.EditValue = _Invoice.SupplierId;
                    TransactionTypeIdTextEdit.EditValue = _Invoice.TranTypeId;
                }
               
                AccSearchLookUpEdit.ReadOnly = true;
                AccSearchLookUpEdit.ReadOnly = true;

            }
            LoadBindings();
        }
        

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            gvwItems.AddNewRow();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // add
            if (_Invoice == null)
            {
                try
                {

                  

                    if (!SetData())
                        return;
                
                    gvwItems.RefreshData();
                    gridControl1.RefreshDataSource();
                    MyInformation.Msg.ShowOkMessage();
                    btnSave.Enabled = false;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MyInformation.Msg.ShowErrorMessageWithLog(ex);
                }
            }

            //edit
            else
            {
                try
                {
                    if (_Invoice.Date.AddDays(1) > DateTime.Today)
                    {
                        MyInformation.Msg.ShowWarningMessage("Date Expired !");
                    }
                    else
                    {
                        _transactionType = (int)TransactionTypeIdTextEdit.EditValue;

                        if (!EditData())
                            return;

                        gvwItems.RefreshData();
                        gridControl1.RefreshDataSource();
                        MyInformation.Msg.ShowOkMessage();
                        btnSave.Enabled = false;
                        this.Close();
                    }

                    
                }
                catch (Exception ex)
                {
                    MyInformation.Msg.ShowErrorMessageWithLog(ex);
                }
            }
        }
        private bool SetData()
        {
           

            using var dbContextTransaction = _ctx.Database.BeginTransaction();
            
            try
            {
                Validate();
                BdgInvoice.EndEdit();

                #region Validation

                var errors = false;

              

                if (string.IsNullOrEmpty(DateDateEdit.Text))
                {
                    errors = true;
                    DateDateEdit.ErrorText = Resources.EmptyData;
                }

                if (string.IsNullOrEmpty(AccSearchLookUpEdit.Text))
                {
                    AccSearchLookUpEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }

                if (string.IsNullOrEmpty(TransactionTypeIdTextEdit.Text))
                {
                    TransactionTypeIdTextEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }

                bool isempty=false;
                foreach (var item in _ctx.SupplierTransactionsItem.Local.ToList())
                {

                    if (item.Qty == 0)
                    isempty = true;
                    
                  
                }

                if (isempty)
                {

                    MyInformation.Msg.ShowWarningMessage("Check QTY !");
                    errors = true;
                }

                if (errors)
                    return false;

                #endregion Validation


                _transactionType = (int)TransactionTypeIdTextEdit.EditValue;
                // Add Invoice
                if (_transactionType == (int)MyTypes.Transactiontypes.PurchasInvoice ||
                    _transactionType == (int)MyTypes.Transactiontypes.PurchaseOrder
                    || _transactionType == (int)MyTypes.Transactiontypes.AdjustIn ||
                    _transactionType == (int)MyTypes.Transactiontypes.ReturnPurchase
                    || _transactionType == (int)MyTypes.Transactiontypes.OpeningQTY)
                {
                    SupplierTransaction Invoice = new SupplierTransaction();

                    Invoice.Date = DateDateEdit.DateTime;
                    Invoice.SupplierId = (int)AccSearchLookUpEdit.EditValue;
                    Invoice.Notes = NotesTextEdit.Text;
                    Invoice.TranTypeId = (int)TransactionTypeIdTextEdit.EditValue;
                    Invoice.Item = _ctx.SupplierTransactionsItem.Local.ToBindingList();
                    _ctx.SupplierTransactions.Add(Invoice);
                    _ctx.SaveChanges();
                    AutoNbTextEdit.Text = Invoice.AutoNbr.ToString();


                    // Get the new items and add the new Qty
                    var items = _ctx.SupplierTransactionsItem
                        .Include(s => s.Product)
                        .Where(v => v.Invoice.AutoNbr == Invoice.AutoNbr)
                        .ToList();

                    foreach (var item in items)
                    {



                        var p = _ctx.Stocks.FirstOrDefault(s => s.ItemBarcode == item.ProductId);

                        // PURCHASE INVOICE
                        if (_transactionType == (int)MyTypes.Transactiontypes.PurchasInvoice)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty + qty;
                            var price = item.Itemprice;
                            p.Itemprice = price;
                            _ctx.SaveChanges();

                        }
                        // PURCHASE ORDER
                        if (_transactionType == (int)MyTypes.Transactiontypes.PurchaseOrder)
                        {

                        }
                        // Adjust IN
                        if (_transactionType == (int)MyTypes.Transactiontypes.AdjustIn)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty + qty;
                            _ctx.SaveChanges();
                        }
                        // opening QTY
                        if (_transactionType == (int)MyTypes.Transactiontypes.OpeningQTY)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty + qty;
                            _ctx.SaveChanges();
                        }

                        // PURCHASE RETURN
                        if (_transactionType == (int)MyTypes.Transactiontypes.ReturnPurchase)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty - qty;
                            _ctx.SaveChanges();
                        }
                       
                    }



                    _ctx.SaveChanges();
                    dbContextTransaction.Commit();

                    return true;
                }
                else
                {
                    MyInformation.Msg.ShowWarningMessage("Invalid Transaction Type !");
                    return false;
                }
              
            }
            catch (DbEntityValidationException ex)
            {
                dbContextTransaction.Rollback();
                var newException = ex;
                MyInformation.Msg.ShowErrorMessageWithLog(newException);
                return false;
            }
            catch (Exception ex)
            {
                dbContextTransaction.Rollback();
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
                return false;
            }
        }

        private bool EditData()
        {


            using var dbContextTransaction = _ctx.Database.BeginTransaction();

            try
            {
                Validate();
                BdgInvoice.EndEdit();

                #region Validation

                var errors = false;



                if (string.IsNullOrEmpty(DateDateEdit.Text))
                {
                    errors = true;
                    DateDateEdit.ErrorText = Resources.EmptyData;
                }

                if (string.IsNullOrEmpty(AccSearchLookUpEdit.Text))
                {
                    AccSearchLookUpEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }

                if (string.IsNullOrEmpty(TransactionTypeIdTextEdit.Text))
                {
                    TransactionTypeIdTextEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }


                bool isempty = false;
                foreach (var item in _ctx.SupplierTransactionsItem.Local.ToList())
                {

                    if (item.Qty == 0)
                        isempty = true;


                }

                if (isempty)
                {

                    MyInformation.Msg.ShowWarningMessage("Check QTY !");
                    errors = true;
                }

                if (errors)
                    return false;

                #endregion Validation


                // Edit Invoice
                
                _Invoice.Notes = NotesTextEdit.Text;
                _Invoice.Item = _ctx.SupplierTransactionsItem.Local.ToBindingList();
               _ctx.SaveChanges();



                // Get the new items and add the new Qty
                var items = _ctx.SupplierTransactionsItem
                    .Include(s => s.Product)
                    .Where(v => v.Invoice.AutoNbr == _invoiceId)
                    .ToList();
                if (_transactionType == (int)MyTypes.Transactiontypes.PurchasInvoice
                    || _transactionType == (int)MyTypes.Transactiontypes.PurchaseOrder
                    || _transactionType == (int)MyTypes.Transactiontypes.ReturnPurchase)
                {
                    foreach (var item in items)
                    {


                        var p = _ctx.Stocks.FirstOrDefault(s => s.ItemBarcode == item.ProductId);

                        // PURCHASE INVOICE
                        if (_transactionType == (int)MyTypes.Transactiontypes.PurchasInvoice)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty + qty;
                         
                            var price = item.Itemprice;
                            p.Itemprice = price;
                            _ctx.SaveChanges();

                        }
                        // PURCHASE ORDER
                        if (_transactionType == (int)MyTypes.Transactiontypes.PurchaseOrder)
                        {

                        }
                        // PURCHASE RETURN
                        if (_transactionType == (int)MyTypes.Transactiontypes.ReturnPurchase)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty - qty;
                            _ctx.SaveChanges();
                        }
                      
                    }



                    _ctx.SaveChanges();
                    dbContextTransaction.Commit();
                    //
                    return true;
                }
                else
                {
                    MyInformation.Msg.ShowWarningMessage("Invalid Transaction Type !");
                    return false;
                }

                
            }
            catch (DbEntityValidationException ex)
            {
                dbContextTransaction.Rollback();
                var newException = ex;
                MyInformation.Msg.ShowErrorMessageWithLog(newException);
                return false;
            }
            catch (Exception ex)
            {
                dbContextTransaction.Rollback();
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
                return false;
            }
        }

        private void gvwItems_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (e.Column.VisibleIndex == colProduct.VisibleIndex)
                {
                    if (BdgItems.Position.ToString() == "0")
                    {
                        var itemid = gvwItems.GetRowCellDisplayText(e.RowHandle, colProductId);
                        var s = _ctx.Stocks.Where(s => s.ItemBarcode == itemid).FirstOrDefault();
                        if (s != null)
                        {
                            gvwItems.SetRowCellValue(e.RowHandle, colProductId, itemid);

                            gvwItems.SetRowCellValue(e.RowHandle, ColPrice, s.Itemprice);
                        }

                    }
                    else
                    {
                        var record = _ctx.SupplierTransactionsItem.Local[BdgItems.Position];

                        var item = _ctx.Stocks.Where(s => s.ItemBarcode == record.ProductId).FirstOrDefault();
                        if (item != null)
                        {
                            record.Itemprice = item.Itemprice;
                        }
                        else
                        {
                            record.Itemprice = 0;
                        }

                       
                    }



                }
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
            }

        }
        }
    
}