﻿namespace SKYLAR_MANAGE.Screens.Suppliers
{
    partial class EditSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.Phone_txt = new System.Windows.Forms.TextBox();
            this.Name_txt = new System.Windows.Forms.TextBox();
            this.UserName_lbl = new System.Windows.Forms.Label();
            this.Passlbel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::SKYLAR_MANAGE.Properties.Resources.edit;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(85, 203);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(341, 49);
            this.btnSave.TabIndex = 60;
            this.btnSave.Text = "Edit Supplier";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Phone_txt
            // 
            this.Phone_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Phone_txt.Location = new System.Drawing.Point(208, 110);
            this.Phone_txt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Phone_txt.Name = "Phone_txt";
            this.Phone_txt.Size = new System.Drawing.Size(274, 23);
            this.Phone_txt.TabIndex = 59;
            this.Phone_txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Phone_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Phone_txt_KeyPress);
            // 
            // Name_txt
            // 
            this.Name_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Name_txt.Location = new System.Drawing.Point(208, 50);
            this.Name_txt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name_txt.Name = "Name_txt";
            this.Name_txt.Size = new System.Drawing.Size(274, 23);
            this.Name_txt.TabIndex = 58;
            this.Name_txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UserName_lbl
            // 
            this.UserName_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UserName_lbl.AutoSize = true;
            this.UserName_lbl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName_lbl.Location = new System.Drawing.Point(33, 49);
            this.UserName_lbl.Name = "UserName_lbl";
            this.UserName_lbl.Size = new System.Drawing.Size(159, 24);
            this.UserName_lbl.TabIndex = 56;
            this.UserName_lbl.Text = "Supplier Name";
            this.UserName_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Passlbel
            // 
            this.Passlbel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Passlbel.AutoSize = true;
            this.Passlbel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passlbel.Location = new System.Drawing.Point(33, 108);
            this.Passlbel.Name = "Passlbel";
            this.Passlbel.Size = new System.Drawing.Size(160, 24);
            this.Passlbel.TabIndex = 57;
            this.Passlbel.Text = "Phone Number";
            this.Passlbel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EditSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 293);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.Phone_txt);
            this.Controls.Add(this.Name_txt);
            this.Controls.Add(this.UserName_lbl);
            this.Controls.Add(this.Passlbel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.Name = "EditSupplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Supplier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox Phone_txt;
        private System.Windows.Forms.TextBox Name_txt;
        private System.Windows.Forms.Label UserName_lbl;
        private System.Windows.Forms.Label Passlbel;
    }
}