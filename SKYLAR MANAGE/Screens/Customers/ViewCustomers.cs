﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Screens.Suppliers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Functions;

namespace SKYLAR_MANAGE.Screens.Customers
{
    public partial class ViewCustomers : DevExpress.XtraEditors.XtraForm
    {
        public ViewCustomers()
        {
            InitializeComponent();
            MyDbContext ctx = new MyDbContext();
            gridView1.ApplyGridStyle("All Customers", "All Customers", true, append: true,
                  allowDelete: false);
            bindingSource1.DataSource = ctx.CustomersAccounts.ToList();
            LoadBindings();
        }
        private void refreshbtn_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            bindingSource1.DataSource = ctx.CustomersAccounts.ToList();

        }
        public int LoadBindings(bool isRefresh = false)
        {
            try
            {
                var ctx = new MyDbContext();
                repositoryDepSearchLookUpEdit1.MyInitialize2(MyExtensions.Type.Departments, ctx, isRefresh, LoadBindings, showAddButton: false);
                return 1;
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);

                return 0;
            }
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            Boolean frmopen = false;
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                //iterate through
                if (frm.Name == "AddCustomer")
                {
                    frmopen = true;
                }

            }

            if (!frmopen)
            {
                AddCustomer ai = new AddCustomer();
                ai.ShowDialog();
                refreshbtn_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.CustomersAccount customr)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "Customer_View_Invoice")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    Customer_View_Invoice vs = new Customer_View_Invoice(customr.CustomerId);
                    vs.Show();
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}