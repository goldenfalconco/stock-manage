﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Screens.Customers
{
    public partial class AddCustomer : DevExpress.XtraEditors.XtraForm
    {
       
        public AddCustomer()
        {
            InitializeComponent();
          


        }
        public int LoadBindings(bool isRefresh = false)
        {
            try
            {
                var ctx = new MyDbContext();
                DepSearchlookup.MyInitialize(MyExtensions.Type.Departments, ctx, isRefresh, LoadBindings, showAddButton: false);
               
                return 1;
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);

                return 0;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            var usr = ctx.CustomersAccounts.FirstOrDefault(s => s.CustomerName.ToLower().Trim().Equals(Name_txt.Text.ToLower()));


            if (usr != null)
            {
                var result = MessageBox.Show("Account already Exist !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else
            {

                if (Name_txt.Text != "")
                {
                    CustomersAccount newAccount = new CustomersAccount();
                    newAccount.CustomerName = Name_txt.Text;
                    newAccount.CustomerPhone = Phone_txt.Text;
                    newAccount.DepartmentId = Convert.ToInt32(DepSearchlookup.EditValue.ToString());

                    ctx.CustomersAccounts.Add(newAccount);

                    ctx.SaveChanges();
                    var result = MessageBox.Show("Account Added Successfully !", Properties.Resources.AppName,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    if (result == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    if (Name_txt.Text == "")
                    {

                        Name_txt.ErrorText = Resources.EmptyData;
                        Name_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    }
                }

            }
        }

        private void Phone_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            //only Numbers
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void AddCustomer_Load(object sender, EventArgs e)
        {
            LoadBindings();
        }
    }
}