﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Screens.Suppliers;
using SKYLAR_MANAGE.Reports;
using DevExpress.XtraReports.UI;
using SKYLAR_MANAGE.ReportFotm;
using DevExpress.XtraSplashScreen;
using CodeTecLib;

namespace SKYLAR_MANAGE.Screens.Customers
{
  
    public partial class Customer_View_Invoice : DevExpress.XtraEditors.XtraForm
    {
        private int _CustomerId;
        public Customer_View_Invoice(int customerId)
        {
            InitializeComponent();
           
            _CustomerId = customerId;
            MyDbContext ctx = new MyDbContext();
            gridView1.ApplyGridStyle("Invoices", "Invoices", true, append: true,
                 allowDelete: false);
            bindingSource1.DataSource = ctx.CustomerTransactions.Where(s => s.CustomerId == _CustomerId).ToList();
        
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.CustomerTransaction Customertran)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "NewCustomerInvoice")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    NewCustomerInvoice vs = new NewCustomerInvoice(Customertran.AutoNbr);
                    vs.Show();
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Addpo_Click(object sender, EventArgs e)
        {
            var f = new SlcDate();
            f.ShowDialog();
            if (f.DialogResult != DialogResult.OK)
                return;

         
            var FromD = f.FromD;
            var ToD = f.ToD;
           
            bool hasRows = false;
            var report = new ReportCustomerTransactionsItems(_CustomerId, FromD,ToD,ref hasRows);
            using var rpt = new ReportPrintTool(report);
            if (!hasRows)
            {

                MyInformation.Msg.ShowWarningMessage("No Data Found !");
                return;
            }
          
            report.CreateDocument(false);
            rpt.ShowPreviewDialog();
        }

        private void bindingSource2_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (gridView1.GetFocusedRow() is not Models.CustomerTransaction Customertran)
                return;
            bool hasRows = false;
            var report = new Invoice(Customertran.AutoNbr, ref hasRows);
            using var rpt = new ReportPrintTool(report);
            if (!hasRows)
            {

                MyInformation.Msg.ShowWarningMessage("No Data Found !");
                return;
            }

            report.CreateDocument(false);
            rpt.ShowPreviewDialog();
        }
    }
}