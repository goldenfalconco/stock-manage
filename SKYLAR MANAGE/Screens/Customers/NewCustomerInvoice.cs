﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Screens.Customers
{
    public partial class NewCustomerInvoice : DevExpress.XtraEditors.XtraForm
    {

        private int _invoiceId;

        private MyDbContext _ctx;
        private CustomerTransaction _Invoice;
        private List<CustomerTransactionItem> ItemsList;
        private int _transactionType;
        public NewCustomerInvoice(int invoiceid = 0)
        {
            InitializeComponent();
            _ctx = new MyDbContext();
            _invoiceId = invoiceid;
        }
        public int LoadBindings(bool isRefresh = false)
        {
            try
            {
                var ctx = new MyDbContext();
                AccSearchLookUpEdit.MyInitialize(MyExtensions.Type.Customer, ctx, isRefresh, LoadBindings, showAddButton: true);
                TransactionTypeIdTextEdit.MyInitialize(MyExtensions.Type.Transaction, ctx, isRefresh, LoadBindings, showAddButton: false);
                repositoryItemSearchLookUpItems.MyInitialize2(MyExtensions.Type.stock, ctx, isRefresh, LoadBindings, showAddButton: true);
                gvwItems.ApplyGridStyle("FrmCustomerInvoice", "Items", true, append: true,
                    allowDelete: true);
                gvwItems.AddNewRow();
                return 1;
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);

                return 0;
            }
        }

        private void NewCustomerInvoice_Load(object sender, EventArgs e)
        {
            if (_invoiceId == 0)
            {
                BdgItems.DataSource = _ctx.CustomerTransactionItems.Local.ToBindingList();
                DateDateEdit.DateTime = DateTime.Today;
            }
            //edit
            else
            {
                _ctx.CustomerTransactionItems.Where(v => v.Invoice.AutoNbr == _invoiceId).Load();
                BdgItems.DataSource = _ctx.CustomerTransactionItems.Local.ToBindingList();
                _Invoice = _ctx.CustomerTransactions.Find(_invoiceId);
                if (_Invoice != null)
                {
                    DateDateEdit.DateTime = _Invoice.Date;
                    AutoNbTextEdit.Text = _Invoice.AutoNbr.ToString();
                    NotesTextEdit.Text = _Invoice.Notes;
                    AccSearchLookUpEdit.EditValue = _Invoice.CustomerId;
                    TransactionTypeIdTextEdit.EditValue = _Invoice.TranTypeId;
                  
                }

                AccSearchLookUpEdit.ReadOnly = true;
                AccSearchLookUpEdit.ReadOnly = true;

            }
            LoadBindings();
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            gvwItems.AddNewRow();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // add
            if (_Invoice == null)
            {
                try
                {



                    if (!SetData())
                        return;

                    gvwItems.RefreshData();
                    gridControl1.RefreshDataSource();
                    MyInformation.Msg.ShowOkMessage();
                    btnSave.Enabled = false;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MyInformation.Msg.ShowErrorMessageWithLog(ex);
                }
            }

            //edit
            else
            {
                try
                {
                    if (_Invoice.Date < DateTime.Today)
                    {
                        MyInformation.Msg.ShowWarningMessage("Date Expired !");
                    }
                    else
                    {
                        _transactionType = (int)TransactionTypeIdTextEdit.EditValue;

                        if (!EditData())
                            return;

                        gvwItems.RefreshData();
                        gridControl1.RefreshDataSource();
                        MyInformation.Msg.ShowOkMessage();
                        btnSave.Enabled = false;
                        this.Close();
                    }


                }
                catch (Exception ex)
                {
                    MyInformation.Msg.ShowErrorMessageWithLog(ex);
                }
            }
        }
        private bool SetData()
        {


            using var dbContextTransaction = _ctx.Database.BeginTransaction();

            try
            {
                Validate();
                BdgInvoice.EndEdit();

                #region Validation

                var errors = false;



                if (string.IsNullOrEmpty(DateDateEdit.Text))
                {
                    errors = true;
                    DateDateEdit.ErrorText = Resources.EmptyData;
                }

                if (string.IsNullOrEmpty(AccSearchLookUpEdit.Text))
                {
                    AccSearchLookUpEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }

                if (string.IsNullOrEmpty(TransactionTypeIdTextEdit.Text))
                {
                    TransactionTypeIdTextEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }

                bool isempty = false;
                foreach (var item in _ctx.CustomerTransactionItems.Local.ToList())
                {

                    if (item.Qty == 0)
                        isempty = true;


                }

                if (isempty)
                {

                    MyInformation.Msg.ShowWarningMessage("Check QTY !");
                    errors = true;
                }

                if (errors)
                    return false;

                #endregion Validation

                _transactionType = (int)TransactionTypeIdTextEdit.EditValue;
                // Add Invoice
                // Add Invoice
                if (_transactionType == (int)MyTypes.Transactiontypes.SaleInvoice ||
                    _transactionType == (int)MyTypes.Transactiontypes.SaleOrder
                    || _transactionType == (int)MyTypes.Transactiontypes.AdjustOut ||
                    _transactionType == (int)MyTypes.Transactiontypes.ReturnSales)
                {
                    CustomerTransaction Invoice = new CustomerTransaction();
                    int cid = (int)AccSearchLookUpEdit.EditValue;
                    Models.CustomersAccount cust = _ctx.CustomersAccounts.Find(cid);
                    Invoice.DepartmentId = cust.DepartmentId;
                    Invoice.Date = DateDateEdit.DateTime;
                    Invoice.CustomerId = (int)AccSearchLookUpEdit.EditValue;
                    Invoice.Notes = NotesTextEdit.Text;
                    Invoice.TranTypeId = (int)TransactionTypeIdTextEdit.EditValue;
                    Invoice.Item = _ctx.CustomerTransactionItems.Local.ToBindingList();
                  
                    _ctx.CustomerTransactions.Add(Invoice);
                    _ctx.SaveChanges();
                    AutoNbTextEdit.Text = Invoice.AutoNbr.ToString();


                    // Get the new items and add the new Qty
                    var items = _ctx.CustomerTransactionItems
                        .Include(s => s.Product)
                        .Where(v => v.Invoice.AutoNbr == Invoice.AutoNbr)
                        .ToList();

                    foreach (var item in items)
                    {



                        var p = _ctx.Stocks.FirstOrDefault(s => s.ItemBarcode == item.ProductId);

                        // SALE INVOICE
                        if (_transactionType == (int)MyTypes.Transactiontypes.SaleInvoice)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty - qty;
                            _ctx.SaveChanges();

                        }
                        // SALE ORDER
                        if (_transactionType == (int)MyTypes.Transactiontypes.SaleOrder)
                        {

                        }
                        // Adjust OUT
                        if (_transactionType == (int)MyTypes.Transactiontypes.AdjustOut)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty - qty;
                            _ctx.SaveChanges();
                        }
                        

                        // SALE RETURN
                        if (_transactionType == (int)MyTypes.Transactiontypes.ReturnSales)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty + qty;
                            _ctx.SaveChanges();
                        }
                    }

                   

                    _ctx.SaveChanges();
                    dbContextTransaction.Commit();

                    return true;
                }
                else
                {
                    MyInformation.Msg.ShowWarningMessage("Invalid Transaction Type !");
                    return false;
                }

               
            }
            catch (DbEntityValidationException ex)
            {
                dbContextTransaction.Rollback();
                var newException = ex;
                MyInformation.Msg.ShowErrorMessageWithLog(newException);
                return false;
            }
            catch (Exception ex)
            {
                dbContextTransaction.Rollback();
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
                return false;
            }
        }

        private bool EditData()
        {


            using var dbContextTransaction = _ctx.Database.BeginTransaction();

            try
            {
                Validate();
                BdgInvoice.EndEdit();

                #region Validation

                var errors = false;



                if (string.IsNullOrEmpty(DateDateEdit.Text))
                {
                    errors = true;
                    DateDateEdit.ErrorText = Resources.EmptyData;
                }

                if (string.IsNullOrEmpty(AccSearchLookUpEdit.Text))
                {
                    AccSearchLookUpEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }

                if (string.IsNullOrEmpty(TransactionTypeIdTextEdit.Text))
                {
                    TransactionTypeIdTextEdit.ErrorText = Resources.Required_msg;
                    errors = true;
                }


                bool isempty = false;
                foreach (var item in _ctx.SupplierTransactionsItem.Local.ToList())
                {

                    if (item.Qty == 0)
                        isempty = true;


                }

                if (isempty)
                {

                    MyInformation.Msg.ShowWarningMessage("Check QTY !");
                    errors = true;
                }

                if (errors)
                    return false;

                #endregion Validation


                // Edit Invoice

                _Invoice.Notes = NotesTextEdit.Text;
                _Invoice.Item = _ctx.CustomerTransactionItems.Local.ToBindingList();
                _ctx.SaveChanges();

                CustomerTransaction Invoice = new CustomerTransaction();
                int cid = (int)AccSearchLookUpEdit.EditValue;
                Models.CustomersAccount cust = _ctx.CustomersAccounts.Find(cid);
                Invoice.DepartmentId = cust.DepartmentId;

                // Get the new items and add the new Qty
                var items = _ctx.SupplierTransactionsItem
                    .Include(s => s.Product)
                    .Where(v => v.Invoice.AutoNbr == _invoiceId)
                    .ToList();

                if (_transactionType == (int)MyTypes.Transactiontypes.SaleInvoice
                    || _transactionType == (int)MyTypes.Transactiontypes.SaleOrder
                    || _transactionType == (int)MyTypes.Transactiontypes.ReturnSales)
                {
                    foreach (var item in items)
                    {


                        var p = _ctx.Stocks.FirstOrDefault(s => s.ItemBarcode == item.ProductId);

                        // SALE INVOICE
                        if (_transactionType == (int)MyTypes.Transactiontypes.SaleInvoice)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty + qty;
                            _ctx.SaveChanges();

                        }
                        // SALE ORDER
                        if (_transactionType == (int)MyTypes.Transactiontypes.SaleOrder)
                        {

                        }
                        // SALE RETURN
                        if (_transactionType == (int)MyTypes.Transactiontypes.ReturnSales)
                        {
                            var qty = item.Qty;
                            var oldqty = p.ItemAvailableQty;
                            p.ItemAvailableQty = oldqty - qty;
                            _ctx.SaveChanges();
                        }
                    }



                    _ctx.SaveChanges();
                    dbContextTransaction.Commit();

                    return true;
                }
                else
                {
                    MyInformation.Msg.ShowWarningMessage("Invalid Transaction Type !");
                    return false;
                }
               
            }
            catch (DbEntityValidationException ex)
            {
                dbContextTransaction.Rollback();
                var newException = ex;
                MyInformation.Msg.ShowErrorMessageWithLog(newException);
                return false;
            }
            catch (Exception ex)
            {
                dbContextTransaction.Rollback();
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
                return false;
            }
        }
    }
}