﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Screens.Stock
{
    public partial class EditItem : DevExpress.XtraEditors.XtraForm
    {

        MyDbContext ctx = new MyDbContext();
        Models.Stock item = new Models.Stock();

        public EditItem(string ItemId)
        {
            InitializeComponent();
         
          
            item = ctx.Stocks.FirstOrDefault(s => s.ItemBarcode== ItemId);
            if (item != null)
            {
                name_txt.Text = item.ItemName;
                barcode_txt.Text = item.ItemBarcode.ToString();
                qty_txt.Text = item.ItemAvailableQty.ToString();
                Itemprice_txt.Text = item.Itemprice.ToString();
            }
            else
            {
                var result = MessageBox.Show("Item Not Exist !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                if (result == DialogResult.OK)
                {

                    this.Close();
                }
            }
          
        }

    

        private void btnSave_Click(object sender, EventArgs e)
        {
            item.ItemName = name_txt.Text;
            item.ItemAvailableQty = double.Parse(qty_txt.Text);
            item.Itemprice = double.Parse(Itemprice_txt.Text);
            ctx.SaveChanges();
            var result = MessageBox.Show("Updated Successfully !", Properties.Resources.AppName,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            if (result == DialogResult.OK)
            {

                this.Close();
            }
        }
    }
}