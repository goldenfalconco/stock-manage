﻿namespace SKYLAR_MANAGE.Screens.Stock
{
    partial class AddItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.name_txt = new DevExpress.XtraEditors.TextEdit();
            this.barcode_txt = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.qty_txt = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.suppliersAccountBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.name_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcode_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.suppliersAccountBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // name_txt
            // 
            this.name_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.name_txt.Location = new System.Drawing.Point(135, 80);
            this.name_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.name_txt.Name = "name_txt";
            this.name_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.name_txt.Properties.Appearance.Options.UseFont = true;
            this.name_txt.Properties.Appearance.Options.UseForeColor = true;
            this.name_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.name_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.name_txt.Size = new System.Drawing.Size(293, 24);
            this.name_txt.TabIndex = 1;
            // 
            // barcode_txt
            // 
            this.barcode_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.barcode_txt.Location = new System.Drawing.Point(166, 27);
            this.barcode_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barcode_txt.Name = "barcode_txt";
            this.barcode_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barcode_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.barcode_txt.Properties.Appearance.Options.UseFont = true;
            this.barcode_txt.Properties.Appearance.Options.UseForeColor = true;
            this.barcode_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.barcode_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.barcode_txt.Properties.ReadOnly = true;
            this.barcode_txt.Size = new System.Drawing.Size(261, 24);
            this.barcode_txt.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 19);
            this.label4.TabIndex = 23;
            this.label4.Text = "Item Barcode";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 19);
            this.label3.TabIndex = 21;
            this.label3.Text = "Item Name";
            // 
            // qty_txt
            // 
            this.qty_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.qty_txt.EditValue = "0";
            this.qty_txt.Location = new System.Drawing.Point(157, 122);
            this.qty_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.qty_txt.Name = "qty_txt";
            this.qty_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.qty_txt.Properties.Appearance.Options.UseFont = true;
            this.qty_txt.Properties.Appearance.Options.UseForeColor = true;
            this.qty_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.qty_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.qty_txt.Properties.ReadOnly = true;
            this.qty_txt.Size = new System.Drawing.Size(271, 24);
            this.qty_txt.TabIndex = 2;
            this.qty_txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 19);
            this.label1.TabIndex = 26;
            this.label1.Text = "Available QTY";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(106)))), ((int)(((byte)(225)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::SKYLAR_MANAGE.Properties.Resources.add;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(81, 198);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(292, 40);
            this.btnSave.TabIndex = 56;
            this.btnSave.Text = "Add New Item";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // suppliersAccountBindingSource
            // 
            this.suppliersAccountBindingSource.DataSource = typeof(SKYLAR_MANAGE.Models.Stock);
            // 
            // AddItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 249);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.qty_txt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.name_txt);
            this.Controls.Add(this.barcode_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "AddItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Item";
            this.Load += new System.EventHandler(this.AddItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.name_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcode_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qty_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.suppliersAccountBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.TextEdit name_txt;
        private DevExpress.XtraEditors.TextEdit barcode_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit qty_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.BindingSource suppliersAccountBindingSource;
    }
}