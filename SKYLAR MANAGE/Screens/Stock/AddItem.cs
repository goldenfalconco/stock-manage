﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKYLAR_MANAGE.Properties;
using SKYLAR_MANAGE.Models;

using SKYLAR_MANAGE.Functions;
using System.Data.Entity;

namespace SKYLAR_MANAGE.Screens.Stock
{
    public partial class AddItem : DevExpress.XtraEditors.XtraForm
    {

        MyDbContext ctx = new MyDbContext();
        public AddItem()
        {
            InitializeComponent();

        
            
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //only Numbers
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        

        private void btnSave_Click(object sender, EventArgs e)
        {

            Models.Stock s = new Models.Stock();


            if (barcode_txt.Text != "" && name_txt.Text != "" && qty_txt.Text != "")
            {
              
                s.ItemBarcode =barcode_txt.Text;
                s.ItemName = name_txt.Text;
                s.ItemAvailableQty = double.Parse(qty_txt.Text);
            //    s.SupplierId = Convert.ToInt32(SupplierSearchLookUpEdit.EditValue.ToString());
                ctx.Stocks.Add(s);
                ctx.SaveChanges();

                var result = MessageBox.Show("Item Added Successfully !", Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                if (result == DialogResult.OK)
                {

                    AddItem_Load(sender, e);
                 
                    name_txt.Text = "";
                    qty_txt.Text= "0";
                    // this.Close();


                }

            }
            else
            {
                if (barcode_txt.Text == "")
                {
                    barcode_txt.ErrorText = Resources.EmptyData;
                    barcode_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                }

                if (name_txt.Text == "")
                {
                    name_txt.ErrorText = Resources.EmptyData;
                    name_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                }
                if (qty_txt.Text == "")
                {
                    qty_txt.ErrorText = Resources.EmptyData;
                    qty_txt.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                }

                //if (SupplierSearchLookUpEdit.Text == "")
                //{
                //    SupplierSearchLookUpEdit.ErrorText = Resources.EmptyData;
                //    SupplierSearchLookUpEdit.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                //}


            }
        }

        private void AddItem_Load(object sender, EventArgs e)
        {

            MyDbContext ctx = new MyDbContext();
            var Barcode = ctx.Barcodes.FirstOrDefault(s => s.Barcodeid == 1);
            int Itembarcode = Barcode.BarcodeValue;
            barcode_txt.Text = Itembarcode.ToString();

            Barcode.BarcodeValue = int.Parse(barcode_txt.Text) + 1;
            ctx.SaveChanges();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}