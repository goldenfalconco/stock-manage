﻿namespace SKYLAR_MANAGE.Screens.Stock
{
    partial class EditItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qty_txt = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.name_txt = new DevExpress.XtraEditors.TextEdit();
            this.barcode_txt = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.Itemprice_txt = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.qty_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.name_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcode_txt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Itemprice_txt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // qty_txt
            // 
            this.qty_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.qty_txt.Location = new System.Drawing.Point(159, 176);
            this.qty_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.qty_txt.Name = "qty_txt";
            this.qty_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qty_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.qty_txt.Properties.Appearance.Options.UseFont = true;
            this.qty_txt.Properties.Appearance.Options.UseForeColor = true;
            this.qty_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.qty_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.qty_txt.Properties.ReadOnly = true;
            this.qty_txt.Size = new System.Drawing.Size(271, 24);
            this.qty_txt.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 19);
            this.label1.TabIndex = 36;
            this.label1.Text = "Available QTY";
            // 
            // name_txt
            // 
            this.name_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.name_txt.Location = new System.Drawing.Point(137, 93);
            this.name_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.name_txt.Name = "name_txt";
            this.name_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.name_txt.Properties.Appearance.Options.UseFont = true;
            this.name_txt.Properties.Appearance.Options.UseForeColor = true;
            this.name_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.name_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.name_txt.Size = new System.Drawing.Size(293, 24);
            this.name_txt.TabIndex = 29;
            // 
            // barcode_txt
            // 
            this.barcode_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.barcode_txt.Location = new System.Drawing.Point(169, 41);
            this.barcode_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barcode_txt.Name = "barcode_txt";
            this.barcode_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barcode_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.barcode_txt.Properties.Appearance.Options.UseFont = true;
            this.barcode_txt.Properties.Appearance.Options.UseForeColor = true;
            this.barcode_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.barcode_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.barcode_txt.Properties.ReadOnly = true;
            this.barcode_txt.Size = new System.Drawing.Size(261, 24);
            this.barcode_txt.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 19);
            this.label4.TabIndex = 35;
            this.label4.Text = "Item Barcode";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 19);
            this.label3.TabIndex = 34;
            this.label3.Text = "Item Name";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::SKYLAR_MANAGE.Properties.Resources.edit;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(75, 224);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(292, 40);
            this.btnSave.TabIndex = 61;
            this.btnSave.Text = "Edit Item";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Itemprice_txt
            // 
            this.Itemprice_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Itemprice_txt.Location = new System.Drawing.Point(140, 134);
            this.Itemprice_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Itemprice_txt.Name = "Itemprice_txt";
            this.Itemprice_txt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Itemprice_txt.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Itemprice_txt.Properties.Appearance.Options.UseFont = true;
            this.Itemprice_txt.Properties.Appearance.Options.UseForeColor = true;
            this.Itemprice_txt.Properties.Appearance.Options.UseTextOptions = true;
            this.Itemprice_txt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Itemprice_txt.Size = new System.Drawing.Size(293, 24);
            this.Itemprice_txt.TabIndex = 62;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 19);
            this.label2.TabIndex = 63;
            this.label2.Text = "Item Price";
            // 
            // EditItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 292);
            this.Controls.Add(this.Itemprice_txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.qty_txt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.name_txt);
            this.Controls.Add(this.barcode_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "EditItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Item";
            ((System.ComponentModel.ISupportInitialize)(this.qty_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.name_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcode_txt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Itemprice_txt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit qty_txt;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit name_txt;
        private DevExpress.XtraEditors.TextEdit barcode_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
        private DevExpress.XtraEditors.TextEdit Itemprice_txt;
        private System.Windows.Forms.Label label2;
    }
}