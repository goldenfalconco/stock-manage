﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using SKYLAR_MANAGE.Screens.Suppliers;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Screens.General;
using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraReports.UI;
using DevExpress.XtraSplashScreen;
using SKYLAR_MANAGE.Reports;

namespace SKYLAR_MANAGE.Screens.Stock
{
    public partial class ViewStock : DevExpress.XtraEditors.XtraForm
    {
        private string ItemId = "";
        public ViewStock()
        {
            InitializeComponent();
            MyDbContext ctx = new MyDbContext();
            stockBindingSource.DataSource = ctx.Stocks.ToList();
            gridView1.ApplyGridStyle("Stock", "Stock Items", true, append: true,
            allowDelete: false);
            gridControl1.ContextMenuStrip = contextMenuStrip1;
        }

        private void additembtn_Click(object sender, EventArgs e)
        {
           
            Boolean frmopen = false;
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                //iterate through
                if (frm.Name == "AddItem")
                {
                    frmopen = true;
                }

            }

            if (!frmopen)
            {
                AddItem ai = new AddItem();
                ai.ShowDialog();
                refreshbtn_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void refreshbtn_Click(object sender, EventArgs e)
        {
            MyDbContext ctx = new MyDbContext();
            stockBindingSource.DataSource = ctx.Stocks.ToList();
            ItemId = "";
        }

        private void Editbtn_Click(object sender, EventArgs e)
        {

            if (gridView1.GetFocusedRow() is not Models.Stock Product)
                return;
            
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "EditItem")
                    {
                        frmopen = true;
                    }
                }

                if (!frmopen) {

                var output = "";
                var input = MyFunctions.InputBox(Application.ProductName, "Password: ", ref output, true);

                if (input != DialogResult.OK)
                    return;

                if (!output.Equals($@"{DateTime.Now.Year}"))
                    return;

                EditItem ai = new EditItem(Product.ItemBarcode);
                 ai.ShowDialog();
                 refreshbtn_Click(sender, e);

                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            
           
          
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {

                ItemId = (sender as GridView).GetFocusedRowCellValue("ItemBarcode").ToString();

            }
            catch (Exception)
            {

            }
        }


        private void editItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetFocusedRow() is not Models.Stock item)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "EditItem")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {
                    var output = "";
                    var input = MyFunctions.InputBox(Application.ProductName, "Password: ", ref output, true);

                    if (input != DialogResult.OK)
                        return;

                    if (!output.Equals($@"{DateTime.Now.Year}{DateTime.Now.Day}01"))
                        return;

                    EditItem ai = new EditItem(item.ItemBarcode);
                    ai.ShowDialog();
                    refreshbtn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void reloadStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshbtn_Click(sender,e);
        }

        private void deleteItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Delete Item ?", Properties.Resources.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (gridView1.GetFocusedRow() is not Models.Stock item)
                        return;
                    Models.Stock s = new Models.Stock();
                    MyDbContext ctx = new MyDbContext();
                    s = ctx.Stocks.Find(item.ItemBarcode);
                    ctx.Stocks.Remove(s);
                    ctx.SaveChanges();
                    refreshbtn_Click(sender, e);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
          
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            
            try
            {
                if (gridView1.GetFocusedRow() is not Models.Stock item)
                    return;
                Boolean frmopen = false;
                FormCollection fc = Application.OpenForms;

                foreach (Form frm in fc)
                {
                    //iterate through
                    if (frm.Name == "EditItem")
                    {
                        frmopen = true;
                    }

                }

                if (!frmopen)
                {

                    var output = "";
                    var input = MyFunctions.InputBox(Application.ProductName, "Password: ", ref output, true);

                    if (input != DialogResult.OK)
                        return;

                    if (!output.Equals($@"{DateTime.Now.Year}{DateTime.Now.Day}01"))
                        return;

                    EditItem ai = new EditItem(item.ItemBarcode);
                    ai.ShowDialog();
                    refreshbtn_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Form already Opened !", Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportbtn_Click(object sender, EventArgs e)
        {
            bool hasRows = false;
            var report = new ReportAllStock(ref hasRows);
            using var rpt = new ReportPrintTool(report);
            if (!hasRows)
            {

                MyInformation.Msg.ShowWarningMessage("No Data Found !");
                return;
            }
         
            report.CreateDocument(false);
            rpt.ShowPreviewDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1.GetFocusedRow() is not Models.Stock item)
                return;
            bool hasRows = false;
            var report = new ReportItemStatment(item.ItemBarcode,ref hasRows);
            using var rpt = new ReportPrintTool(report);
            if (!hasRows)
            {

                MyInformation.Msg.ShowWarningMessage("No Data Found !");
                return;
            }

            report.CreateDocument(false);
            rpt.ShowPreviewDialog();
        }

        private void statmentReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            simpleButton1_Click( sender,  e);
        }
    }
}