﻿namespace SKYLAR_MANAGE.ReportFotm
{
    partial class SlcDateDep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.DepSearchlookup = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dteTo = new DevExpress.XtraEditors.DateEdit();
            this.dteFrom = new DevExpress.XtraEditors.DateEdit();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lytFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.lytTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.lytClose = new DevExpress.XtraLayout.LayoutControlItem();
            this.lytExport = new DevExpress.XtraLayout.LayoutControlItem();
            this.spc2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepSearchlookup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.DepSearchlookup);
            this.layoutControl1.Controls.Add(this.dteTo);
            this.layoutControl1.Controls.Add(this.dteFrom);
            this.layoutControl1.Controls.Add(this.btnClose);
            this.layoutControl1.Controls.Add(this.btnExport);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(579, 162);
            this.layoutControl1.TabIndex = 25;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // DepSearchlookup
            // 
            this.DepSearchlookup.EditValue = "";
            this.DepSearchlookup.Location = new System.Drawing.Point(113, 36);
            this.DepSearchlookup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DepSearchlookup.Name = "DepSearchlookup";
            this.DepSearchlookup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DepSearchlookup.Properties.DisplayMember = "DepName";
            this.DepSearchlookup.Properties.PopupView = this.searchLookUpEdit1View;
            this.DepSearchlookup.Properties.ValueMember = "DepId";
            this.DepSearchlookup.Size = new System.Drawing.Size(454, 20);
            this.DepSearchlookup.StyleController = this.layoutControl1;
            this.DepSearchlookup.TabIndex = 65;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.DetailHeight = 284;
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // dteTo
            // 
            this.dteTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dteTo.EditValue = null;
            this.dteTo.Location = new System.Drawing.Point(392, 12);
            this.dteTo.Name = "dteTo";
            this.dteTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteTo.Size = new System.Drawing.Size(175, 20);
            this.dteTo.StyleController = this.layoutControl1;
            this.dteTo.TabIndex = 22;
            // 
            // dteFrom
            // 
            this.dteFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dteFrom.EditValue = null;
            this.dteFrom.Location = new System.Drawing.Point(113, 12);
            this.dteFrom.Name = "dteFrom";
            this.dteFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteFrom.Size = new System.Drawing.Size(174, 20);
            this.dteFrom.StyleController = this.layoutControl1;
            this.dteFrom.TabIndex = 21;
            // 
            // btnClose
            // 
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.multiply;
            this.btnClose.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnClose.Location = new System.Drawing.Point(12, 114);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(196, 36);
            this.btnClose.StyleController = this.layoutControl1;
            this.btnClose.TabIndex = 23;
            this.btnClose.Text = "Close";
            // 
            // btnExport
            // 
            this.btnExport.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnExport.Appearance.Options.UseFont = true;
            this.btnExport.ImageOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.save;
            this.btnExport.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnExport.Location = new System.Drawing.Point(371, 114);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(196, 36);
            this.btnExport.StyleController = this.layoutControl1;
            this.btnExport.TabIndex = 24;
            this.btnExport.Text = "View Report";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lytFrom,
            this.lytTo,
            this.lytClose,
            this.lytExport,
            this.spc2,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(579, 162);
            this.Root.TextVisible = false;
            // 
            // lytFrom
            // 
            this.lytFrom.Control = this.dteFrom;
            this.lytFrom.Location = new System.Drawing.Point(0, 0);
            this.lytFrom.Name = "lytFrom";
            this.lytFrom.Size = new System.Drawing.Size(279, 24);
            this.lytFrom.Text = "From";
            this.lytFrom.TextSize = new System.Drawing.Size(89, 13);
            // 
            // lytTo
            // 
            this.lytTo.Control = this.dteTo;
            this.lytTo.Location = new System.Drawing.Point(279, 0);
            this.lytTo.Name = "lytTo";
            this.lytTo.Size = new System.Drawing.Size(280, 24);
            this.lytTo.Text = "To";
            this.lytTo.TextSize = new System.Drawing.Size(89, 13);
            // 
            // lytClose
            // 
            this.lytClose.Control = this.btnClose;
            this.lytClose.Location = new System.Drawing.Point(0, 102);
            this.lytClose.MaxSize = new System.Drawing.Size(200, 40);
            this.lytClose.MinSize = new System.Drawing.Size(200, 40);
            this.lytClose.Name = "lytClose";
            this.lytClose.Size = new System.Drawing.Size(200, 40);
            this.lytClose.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lytClose.TextSize = new System.Drawing.Size(0, 0);
            this.lytClose.TextVisible = false;
            // 
            // lytExport
            // 
            this.lytExport.Control = this.btnExport;
            this.lytExport.Location = new System.Drawing.Point(359, 102);
            this.lytExport.MaxSize = new System.Drawing.Size(200, 40);
            this.lytExport.MinSize = new System.Drawing.Size(200, 40);
            this.lytExport.Name = "lytExport";
            this.lytExport.Size = new System.Drawing.Size(200, 40);
            this.lytExport.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lytExport.TextSize = new System.Drawing.Size(0, 0);
            this.lytExport.TextVisible = false;
            // 
            // spc2
            // 
            this.spc2.AllowHotTrack = false;
            this.spc2.Location = new System.Drawing.Point(200, 102);
            this.spc2.Name = "spc2";
            this.spc2.Size = new System.Drawing.Size(159, 40);
            this.spc2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.DepSearchlookup;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(559, 24);
            this.layoutControlItem1.Text = "Select Department";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(89, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(559, 54);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // SlcDateDep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 162);
            this.Controls.Add(this.layoutControl1);
            this.IconOptions.Image = global::SKYLAR_MANAGE.Properties.Resources.Logo3;
            this.Name = "SlcDateDep";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Date";
            this.Load += new System.EventHandler(this.SlcDateDep_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DepSearchlookup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dteTo;
        private DevExpress.XtraEditors.DateEdit dteFrom;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem lytFrom;
        private DevExpress.XtraLayout.LayoutControlItem lytTo;
        private DevExpress.XtraLayout.LayoutControlItem lytClose;
        private DevExpress.XtraLayout.LayoutControlItem lytExport;
        private DevExpress.XtraLayout.EmptySpaceItem spc2;
        private DevExpress.XtraEditors.SearchLookUpEdit DepSearchlookup;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}