﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.ReportFotm
{
    public partial class SlcDate : DevExpress.XtraEditors.XtraForm
    {
        public DateTime FromD;
        public DateTime ToD;
        public SlcDate()
        {
            InitializeComponent();
            dteFrom.EditValue = dteTo.EditValue = DateTime.Today;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try

            {

                FromD = dteFrom.DateTime.Date;
                ToD = dteTo.DateTime.Date;

                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessage(ex.Message);
            }

        }
    }
}