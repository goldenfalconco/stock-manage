﻿using DevExpress.XtraEditors;
using SKYLAR_MANAGE.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.ReportFotm
{
    public partial class SlcDateDep : DevExpress.XtraEditors.XtraForm
    {
        public DateTime FromD;
        public DateTime ToD;
        public int depId;
        public SlcDateDep()
        {
            InitializeComponent();
            dteFrom.EditValue = dteTo.EditValue = DateTime.Today;

        }

        public int LoadBindings(bool isRefresh = false)
        {
            try
            {
                var ctx = new MyDbContext();
                DepSearchlookup.MyInitialize(MyExtensions.Type.Departments, ctx, isRefresh, LoadBindings, showAddButton: false);

                return 1;
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);

                return 0;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try

            {

                FromD = dteFrom.DateTime.Date;
                ToD = dteTo.DateTime.Date;
                depId = Convert.ToInt32(DepSearchlookup.EditValue.ToString());
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessage(ex.Message);
            }
        }

        private void SlcDateDep_Load(object sender, EventArgs e)
        {
            LoadBindings();
        }
    }
}