﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SKYLAR_MANAGE.Models;


namespace SKYLAR_MANAGE
{
    public class MyDbContext :DbContext
    {
        public MyDbContext() : base("name=AppConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MyDbContext, Migrations.Configuration>());
        }

        public DbSet<UsersAccounts> UsersAccounts { get; set; }
        public DbSet<UsersPcInfo> UsersPcInfos { get; set; }
        public DbSet<CustomersAccount> CustomersAccounts { get; set; }
        public DbSet<SuppliersAccount> SuppliersAccounts { get; set; }
     
        public DbSet<SupplierTransaction> SupplierTransactions { get; set; }
        public DbSet<SupplierTransactionItem> SupplierTransactionsItem { get; set; }

        public DbSet<CustomerTransaction> CustomerTransactions { get; set; }
        public DbSet<CustomerTransactionItem> CustomerTransactionItems { get; set; }
        public DbSet<TransactionTypes> TransactionTypes { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Barcode> Barcodes { get; set; }
        public virtual DbSet<Privilege> Privilege { get; set; }

        public DbSet<Department> Departments { get; set; }
    }
}
