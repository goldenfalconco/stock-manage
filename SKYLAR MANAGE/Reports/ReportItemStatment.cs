﻿using DevExpress.Office;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Wizards.Labels;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace SKYLAR_MANAGE.Reports
{
    public partial class ReportItemStatment : DevExpress.XtraReports.UI.XtraReport
    {
        private string _productId;
       
        public ReportItemStatment(String ProductId, ref bool hasRows)
        {
            InitializeComponent();
            _productId = ProductId;
            var _ctx = new MyDbContext();
            var StatmentItemList = new List<ItemStatment>();
            var customertransactions = _ctx.CustomerTransactionItems.Where(x => x.ProductId == _productId).OrderByDescending(s=>s.Invoice.Date).ToList();
            var suppliertransactions = _ctx.SupplierTransactionsItem.Where(x => x.ProductId == _productId).OrderByDescending(s => s.Invoice.Date).ToList();
            var p = _ctx.Stocks.Where(s => s.ItemBarcode == _productId).FirstOrDefault();
            var itemname = p.ItemName;
            item_label.Text = _productId+" / "+itemname.ToUpper();
            if (p != null)
            {
                foreach (var customertransaction in customertransactions)
                {
                    var ItemStatment = new ItemStatment();
                    ItemStatment.ItemId = customertransaction.ProductId;
                    ItemStatment.ItemName = p.ItemName;
                    ItemStatment.ItemQty = customertransaction.Qty;
                    var tr = _ctx.TransactionTypes.Where(s => s.TranTypeId == customertransaction.Invoice.TranTypeId).FirstOrDefault();
                    if (tr != null)
                    {
                        ItemStatment.TransactionType = tr.TranTypeDesc;
                    }
                    ItemStatment.Date = customertransaction.Invoice.Date;
                    StatmentItemList.Add(ItemStatment);
                }
                foreach (var supplierTransaction in suppliertransactions)
                {
                    var ItemStatment = new ItemStatment();
                    ItemStatment.ItemId = supplierTransaction.ProductId;
                    ItemStatment.ItemName = p.ItemName;
                    ItemStatment.ItemQty = supplierTransaction.Qty;
                    var tr = _ctx.TransactionTypes.Where(s => s.TranTypeId == supplierTransaction.Invoice.TranTypeId).FirstOrDefault();
                    if (tr != null)
                    {
                        ItemStatment.TransactionType = tr.TranTypeDesc;
                    }
                    ItemStatment.Date = supplierTransaction.Invoice.Date;
                    StatmentItemList.Add(ItemStatment);
                }
            }
            else
            {
                hasRows = false;
            }


            StatmentItemList = StatmentItemList.OrderBy(s=>s.Date).ToList();
            bindingSource1.DataSource = StatmentItemList;
            if(StatmentItemList.Count > 0)
            {
                hasRows = true;
            }
        }

    }

    public class ItemStatment
    {
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public double ItemQty { get; set; }
        public string TransactionType { get; set; }
        public DateTime Date { get; set; }
    

    }
}
