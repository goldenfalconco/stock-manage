﻿using DevExpress.Office;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Wizards.Labels;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using SKYLAR_MANAGE.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows;
namespace SKYLAR_MANAGE.Reports
{

    public partial class ReportSalebyDep : DevExpress.XtraReports.UI.XtraReport
    {
        private DateTime _FDate;
        private DateTime _TDate;
        private int _DepId;
        public ReportSalebyDep(DateTime FDate, DateTime TDate, int DepId, ref bool hasRows)
        {
            InitializeComponent();
            var _ctx = new MyDbContext();

            _FDate = FDate;
            _TDate = TDate;
            _DepId = DepId;
            var StatmentItemList = new List<Item>();

            if (_FDate == _TDate)
            {
                date_label.Text = _FDate.ToString("dd/MM/yyyy");


            }
            else
            {
                date_label.Text = _FDate.ToString("dd/MM/yyyy") + " ---> " + _TDate.ToString("dd/MM/yyyy");

            }
            var currentUser = _ctx.UsersAccounts.FirstOrDefault(l => l.UserId == Settings.Default.LastUserId);
            if (currentUser != null)
            {
                printedby_txt.Text = "Printed By : " + currentUser.Username;
            }
            double t = 0;
            var CustomerTransactions = _ctx.CustomerTransactions.Where(s => s.Date >= _FDate && s.Date <= _TDate && s.DepartmentId==_DepId).ToList();
            foreach (var Transaction in CustomerTransactions)
            {
                var TransactionItems = _ctx.CustomerTransactionItems.Where(s => s.Invoice.AutoNbr == Transaction.AutoNbr).ToList();
                foreach (var TransactionItem in TransactionItems)
                {
                    var p = _ctx.Stocks.Where(s => s.ItemBarcode == TransactionItem.ProductId).FirstOrDefault();
                    var ItemStatment = new Item();
                    ItemStatment.ItemId = TransactionItem.ProductId;
                    ItemStatment.ItemName = p.ItemName;
                    ItemStatment.ItemPrice = p.Itemprice;
                    t += p.Itemprice * TransactionItem.Qty;
                    ItemStatment.ItemQty = TransactionItem.Qty;
                    if (TransactionItem.IsCommand == true)
                    {
                        ItemStatment.type = "Command";
                    }
                    else
                    {
                        ItemStatment.type = "Sortie";
                    }
                    var customer = _ctx.CustomersAccounts.Where(s => s.CustomerId == Transaction.CustomerId).FirstOrDefault();
                    if (customer != null)
                    {
                        ItemStatment.customername = customer.CustomerName;
                        var dep = _ctx.Departments.Where(s => s.DepId == customer.DepartmentId).FirstOrDefault();
                        if (dep != null)
                            ItemStatment.customerDep = dep.DepName;
                    }
                    ItemStatment.Date = TransactionItem.Invoice.Date;
                    StatmentItemList.Add(ItemStatment);
                }
            }
            totallabel.Text = "Total : " + t.ToString() + " $";
            bindingSource1.DataSource = StatmentItemList;
            if (StatmentItemList.Count > 0)
            {
                hasRows = true;
            }
        }
    }

    }

