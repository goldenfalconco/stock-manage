﻿using DevExpress.Office;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Wizards.Labels;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Properties;
using SKYLAR_MANAGE.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace SKYLAR_MANAGE.Reports
{
    public partial class Invoice : DevExpress.XtraReports.UI.XtraReport
    {
        private int _InvoiceId;
        public Invoice(int InvoiceId, ref bool hasRows)
        {
            InitializeComponent();
            _InvoiceId = InvoiceId;
            var _ctx = new MyDbContext();

            var StatmentItemList = new List<ItemStatment>();
            var Invoice = _ctx.CustomerTransactions.Where(x => x.AutoNbr == _InvoiceId).FirstOrDefault();

            if (Invoice != null)
            {
                var currentUser = _ctx.UsersAccounts.FirstOrDefault(l => l.UserId == Settings.Default.LastUserId);
                if(currentUser != null)
                {
                    printedby_txt.Text = "Printed By : " + currentUser.Username;
                }
                var tr = _ctx.TransactionTypes.Where(s=>s.TranTypeId==Invoice.TranTypeId).FirstOrDefault();
                var customer = _ctx.CustomersAccounts.Where(s => s.CustomerId == Invoice.CustomerId).FirstOrDefault();
                var dep = _ctx.Departments.Where(s=>s.DepId==customer.DepartmentId).FirstOrDefault();
                main_label.Text = "IT Office - " + tr.TranTypeDesc +" - " + Invoice.Date.ToShortDateString();
                item_label.Text = customer.CustomerName + " - " + dep.DepName + " / Invoice # " + _InvoiceId.ToString();

                var InvoiceItems = _ctx.CustomerTransactionItems.Where(x => x.Invoice.AutoNbr == _InvoiceId).ToList();
                foreach (var item in InvoiceItems)
                {
                    var p = _ctx.Stocks.Where(s => s.ItemBarcode == item.ProductId).FirstOrDefault();
                    var ItemStatment = new ItemStatment();
                    ItemStatment.ItemId = item.ProductId;
                    ItemStatment.ItemName = p.ItemName;
                    ItemStatment.ItemQty = item.Qty;
                    if (item.IsCommand == true)
                    {
                        ItemStatment.type = "Command";
                    }
                    else
                    {
                        ItemStatment.type = "Sortie";
                    }
                    ItemStatment.Date = Invoice.Date;
                    StatmentItemList.Add(ItemStatment);
                }
            }
            else
            {
                hasRows = false;
            }
          
        
            bindingSource1.DataSource = StatmentItemList;
            if (StatmentItemList.Count > 0)
            {
                hasRows = true;
            }
        }

        public class ItemStatment
        {
            public string ItemId { get; set; }
            public string ItemName { get; set; }
            public double ItemQty { get; set; }
        
            public string type { get; set; }
            public DateTime Date { get; set; }


        }
    }
}
