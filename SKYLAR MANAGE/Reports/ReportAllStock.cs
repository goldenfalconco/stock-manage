﻿using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraReports.UI;
using SKYLAR_MANAGE.Functions;
using System;
using SKYLAR_MANAGE.Models;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows;
namespace SKYLAR_MANAGE.Reports
{
    public partial class ReportAllStock : DevExpress.XtraReports.UI.XtraReport
    {
        private MyDbContext _ctx;
        public ReportAllStock(ref bool hasRows)
        {
            InitializeComponent();

            try
            {



                _ctx = new MyDbContext();

                var Allstock = _ctx.Stocks.ToList();
                bindingSource1.DataSource = Allstock;
                if (Allstock.Count > 0)
                {
                    hasRows = true;
                }
                Count_txt.Text = Allstock.Count.ToString() + " Items";


            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
            }
        }

    }
}
