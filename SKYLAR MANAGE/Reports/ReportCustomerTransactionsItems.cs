﻿using DevExpress.Office;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Wizards.Labels;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace SKYLAR_MANAGE.Reports
{
    public partial class ReportCustomerTransactionsItems : DevExpress.XtraReports.UI.XtraReport
    {
        private int _CstomerId;
        private DateTime _FDate;
        private DateTime _TDate;
        public ReportCustomerTransactionsItems(int CustomerId,DateTime FDate, DateTime TDate, ref bool hasRows)
        {
            InitializeComponent();
            var _ctx = new MyDbContext();
            _CstomerId = CustomerId;
            _FDate = FDate;
            _TDate = TDate;
            var StatmentItemList = new List<ItemStatments>();
            var customer = _ctx.CustomersAccounts.Where(s => s.CustomerId == CustomerId).FirstOrDefault();
           
            if (customer != null)
            {
                var DepName = "";
                var dep = _ctx.Departments.Where(s=>s.DepId == customer.DepartmentId).FirstOrDefault();
                if(dep != null)
                {
                    DepName = dep.DepName;
                }
                if (_FDate == _TDate)
                {
                    Main_label.Text = ( customer.CustomerName+" / "+DepName + " / " ).ToUpper()+ _FDate.ToString("dd/MM/yyyy");


                }
                else
                {
                    Main_label.Text = (customer.CustomerName + " / " + DepName + " / ").ToUpper() + _FDate.ToString("dd/MM/yyyy") + " ---> " + _TDate.ToString("dd/MM/yyyy");

                }
                var CustomerTransactions = _ctx.CustomerTransactions.Where(s=>s.CustomerId == CustomerId).Where(s => s.Date >= _FDate && s.Date <= _TDate).OrderByDescending(s=>s.Date).ToList();
                foreach (var Transaction in CustomerTransactions)
                {
                    var TransactionItems = _ctx.CustomerTransactionItems.Where(s=>s.Invoice.AutoNbr==Transaction.AutoNbr).ToList();
                    foreach(var TransactionItem in TransactionItems)
                    {
                        var p = _ctx.Stocks.Where(s => s.ItemBarcode == TransactionItem.ProductId).FirstOrDefault();
                        var ItemStatment = new ItemStatments();
                        ItemStatment.ItemId = TransactionItem.ProductId;
                        ItemStatment.ItemName = p.ItemName;
                        ItemStatment.ItemQty = TransactionItem.Qty;
                        var tr = _ctx.TransactionTypes.Where(s => s.TranTypeId == TransactionItem.Invoice.TranTypeId).FirstOrDefault();
                        if (tr != null)
                        {
                            ItemStatment.TransactionType = tr.TranTypeDesc;
                        }
                        ItemStatment.Invoice = TransactionItem.Invoice.AutoNbr.ToString();
                        ItemStatment.Date = TransactionItem.Invoice.Date;
                        StatmentItemList.Add(ItemStatment);
                    }
                }
                bindingSource1.DataSource = StatmentItemList;
                if (StatmentItemList.Count > 0)
                {
                    hasRows = true;
                }
            }
            else
            {
                hasRows = false;
            }
           

        }

    }
    public class ItemStatments
    {
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public double ItemQty { get; set; }
        public string TransactionType { get; set; }
        public string Invoice { get; set; }
        public DateTime Date { get; set; }


    }
}
