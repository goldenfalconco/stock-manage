﻿using DevExpress.Data;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace SKYLAR_MANAGE.Tools
{
    public static class MyHelperExtensions
    {
        public static T AddIfNotExists<T>(this DbSet<T> dbSet, T entity, Expression<Func<T, bool>> predicate = null)
            where T : class, new()
        {
            var exists = predicate != null ? dbSet.Any(predicate) : dbSet.Any();
            return !exists ? dbSet.Add(entity) : null;
        }

        public static void ApplyEndEditEvent(this BindingSource bindingSource)
        {
            bindingSource.CurrentItemChanged += (_, _) => { bindingSource.EndEdit(); };
        }

        public static T Clone<T>(this T source)
        {
            // Don't serialize a null object, simply return the default for that object
            if (ReferenceEquals(source, null)) return default;

            // initialize inner objects individually
            // for example in default constructor some list property initialized with some values,
            // but in 'source' these items are cleaned -
            // without ObjectCreationHandling.Replace default constructor values will be added to result
            var deserializeSettings = new JsonSerializerSettings
                { ObjectCreationHandling = ObjectCreationHandling.Replace };

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source), deserializeSettings);
        }

        public static T CloneObject<T>(this T source, DbContext context)
        {
            return (T)context.Entry(source).CurrentValues.ToObject();
        }

        public static bool HasProperty(this Type obj, string propertyName)
        {
            return obj.GetProperty(propertyName) != null;
        }

        public static bool HasUnsavedChanges(this DbContext context)
        {
            return ((IObjectContextAdapter)context).ObjectContext.ObjectStateManager
                .GetObjectStateEntries(EntityState.Modified).Any() ||
                context.ChangeTracker.HasChanges();
        }

        public static RepositoryItemButtonEdit SetViewImage(this GridView gridView, string colName, string caption = "Image")
        {
            var unbColumn = gridView.Columns.AddField(colName);
            unbColumn.VisibleIndex = gridView.VisibleColumns.Count;
            unbColumn.UnboundType = UnboundColumnType.Object;
            unbColumn.Caption = caption;

            var edit = new RepositoryItemButtonEdit
            {
                TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
            };
            edit.LookAndFeel.UseDefaultLookAndFeel = false;
            edit.LookAndFeel.Style = LookAndFeelStyle.Skin;
            edit.LookAndFeel.SkinName = "DevExpress Style";
            edit.Buttons[0].Caption = @"View";
            edit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            edit.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;

            // if (gridView.Columns[colName] != null)
            gridView.Columns[colName].ColumnEdit = edit;

            return edit;
        }

        public static string SplitOnCapitalLetter(this string text)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return $"{r.Replace(text, " ")}";
        }

        public static List<dynamic> ToDynamic(this DataTable dt)
        {
            var dynamicDt = new List<dynamic>();
            foreach (DataRow row in dt.Rows)
            {
                dynamic dyn = new ExpandoObject();
                dynamicDt.Add(dyn);
                foreach (DataColumn column in dt.Columns)
                {
                    var dic = (IDictionary<string, object>)dyn;
                    dic[column.ColumnName] = row[column];
                }
            }

            return dynamicDt;
        }

        public static string UcWord(this string text)
        {
            return string.Join(" ", text.Split(' ').ToList()
                    .ConvertAll(word =>
                        word.Substring(0, 1).ToUpper() + word.Substring(1)
                    )
                );
        }

        public static IEnumerable<T> UppercaseClassFields<T>(this IList<T> theInstance)
        {
            if (theInstance == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var theItem in theInstance)
            {
                UppercaseClassFields(theItem);
            }

            return theInstance.ToList();
        }

        private static void UppercaseClassFields<T>(T theInstance)
        {
            if (theInstance == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var property in theInstance.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var theValue = property.GetValue(theInstance, null);
                if (theValue is string)
                {
                    property.SetValue(theInstance, ((string)theValue).ToUpper(), null);
                }
            }
        }
    }
}