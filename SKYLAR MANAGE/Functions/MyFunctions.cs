﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity;
using SKYLAR_MANAGE.Properties;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;
using DevExpress.XtraGrid.Menu;

namespace SKYLAR_MANAGE.Functions
{
    public static class MyFunctions
    {
        public static string GetIpAddress()
        {
            try
            {
                var strHostName = Dns.GetHostName();
                var ipEntry = Dns.GetHostEntry(strHostName);
                var addressList = ipEntry.AddressList;

                foreach (var address in addressList)
                {
                    if (address.ToString().Contains("192.168.1"))
                        return address.ToString();
                }

                return addressList[addressList.Length - 1].ToString();
            }
            catch
            {
                return null;
            }
        }
        public static void ApplyGridStyle(this GridView gridView, string fileName, string caption,
           bool editable = false, bool append = false, bool showSearch = true, bool showTitle = true,
           bool showGroupBox = true, bool allowDelete = true, BindingSource bindingSource = null,
           bool showFooter = false)
        {
            if (bindingSource != null)
            {
                gridView.KeyDown += (_, args) =>
                {
                    if (args.KeyCode == Keys.Down && gridView.FocusedRowHandle == gridView.RowCount - 1)
                        bindingSource.AddNew();
                };
            }

            // Disable grid crud
            gridView.OptionsBehavior.AllowAddRows = append ? DefaultBoolean.True : DefaultBoolean.False;
            gridView.OptionsBehavior.AllowDeleteRows = allowDelete ? DefaultBoolean.True : DefaultBoolean.False;

            gridView.OptionsFind.AlwaysVisible = showSearch;
            gridView.OptionsFind.HighlightFindResults = true;
            gridView.OptionsView.ShowFooter = showFooter;

            // Caption
            gridView.OptionsView.ShowViewCaption = showTitle;
            gridView.ViewCaption = caption;
            gridView.Appearance.ViewCaption.TextOptions.HAlignment = HorzAlignment.Near;

            gridView.OptionsView.ShowGroupPanel = showGroupBox;

            gridView.OptionsView.ColumnHeaderAutoHeight = DefaultBoolean.True;

            if (gridView.Columns.Count > 10)
            {
                gridView.OptionsView.ColumnAutoWidth = false;
                gridView.Columns.ToList().ForEach(s => { s.BestFit(); });
            }
            else
            {
                gridView.OptionsView.ColumnAutoWidth = true;
            }

            gridView.OptionsBehavior.Editable = editable;
            if (editable)
            {
                gridView.OptionsBehavior.EditingMode = GridEditingMode.Inplace;
            }
            else
            {
                // Controls whether multiple cells or rows can be selected
                gridView.OptionsSelection.MultiSelect = true;
                gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CellSelect;
            }

            // Enable Navigator
            gridView.GridControl.UseEmbeddedNavigator = true;
            gridView.GridControl.EmbeddedNavigator.Buttons.Append.Visible = append;
            gridView.GridControl.EmbeddedNavigator.Buttons.Remove.Visible = allowDelete;
            gridView.GridControl.EmbeddedNavigator.Buttons.Edit.Visible = editable;
            gridView.GridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = editable;
            gridView.GridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = editable;

            // Hide context menu from header
            gridView.GridControl.MenuManager = null;

            // Copy cell without header
            gridView.OptionsClipboard.CopyColumnHeaders = DefaultBoolean.False;

            // Add freeze to header context
            gridView.PopupMenuShowing += (_, e) =>
            {
                if (e.MenuType != GridMenuType.Column) return;

              
            };

            foreach (GridColumn column in gridView.Columns)
            {
                if (column.FieldName.ToLower().Contains("note") ||
                    column.FieldName.ToLower().Contains("description") ||
                    column.FieldName.ToLower().Contains("message"))
                    column.MinWidth = 200;
            }

            #region Style

         //   var stylePath = $@"{MyInformation.GridStylePath}{fileName}.xml";
            //var layoutExists = File.Exists(stylePath);

            //// Load style if exists
            //if (layoutExists)
            //    gridView.RestoreLayoutFromXml(stylePath);

            #endregion Style

            #region Context

         

           
            // Initialize reset layout
           

          

            // Initialize menu if null
            if (gridView.GridControl.ContextMenuStrip == null)
            {
                var menuStrip = new ContextMenuStrip();
                gridView.GridControl.ContextMenuStrip = menuStrip;
            }
            else
            {
                // Add separator if not null
                gridView.GridControl.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            }

        
            gridView.GridControl.ContextMenuStrip.Items.Add(new ToolStripSeparator());
           

            #endregion Context
        }
        public static string GetMacAddress()
        {
            try
            {
                var mac = string.Empty;
                var mc = new ManagementClass("Win32_NetworkAdapter");
                var moCol = mc.GetInstances();
                foreach (var o in moCol)
                {
                    var mo = (ManagementObject)o;
                    if (mo?["MacAddress"] != null)
                    {
                        mac = mo["MACAddress"].ToString();
                        if (mac != string.Empty)
                            break;
                    }
                }

                return mac;
            }
            catch
            {
                return null;
            }
        }

        public static string GetName()
        {
            try
            {
                var dnsName = Dns.GetHostName();
                return dnsName;
            }
            catch
            {
                return null;
            }
        }

        public static string GetOsFriendlyName()
        {
            try
            {
                var result = string.Empty;
                var searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
                foreach (var o in searcher.Get())
                {
                    var os = (ManagementObject)o;
                    result = os["Caption"].ToString();
                    break;
                }

                return result;
            }
            catch
            {
                return null;
            }
        }

     
        public static DialogResult InputBox(string title, string promptText, ref string value, bool isPassword = false)
        {
            var form = new Form();
            var label = new Label();
            var textBox = new TextBox();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            if (isPassword)
                textBox.UseSystemPasswordChar = true;

            buttonOk.Text = @"OK";
            buttonCancel.Text = @"Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            var dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static bool HaveAccess(MyTypes.Privilege privilegeRef)
        {
            if (Settings.Default.LastUserId == 1)
                return true;

            try
            {
                using var ctx = new MyDbContext();
                var currentUser = ctx.UsersAccounts.FirstOrDefault(l => l.UserId == Settings.Default.LastUserId);

                if (!string.IsNullOrEmpty(currentUser.Privileges))
                {
                    // Get users privileges
                    var privileges = currentUser.Privileges.Split(',').Select(int.Parse).ToList();

                    if (privileges.Count > 0 && privileges.Contains((int)privilegeRef))
                        return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
