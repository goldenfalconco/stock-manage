﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKYLAR_MANAGE.Functions
{
    public static class MyConfigManager
    {

        public static string ModifyConfigurationManager()
        {


            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);



                config.ConnectionStrings.ConnectionStrings["SKYLAR_MANAGE.Properties.Settings.appConnectionString"].ConnectionString = MyConnection.ConString();
                config.ConnectionStrings.ConnectionStrings["AppConnectionString"].ConnectionString = MyConnection.ConString();
                config.Save(ConfigurationSaveMode.Modified, true);

                // To refresh connection string each time else it will use previous connection string
                ConfigurationManager.RefreshSection("connectionStrings");

                return string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return ex.Message;
            }
        }

    }
}
