﻿using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;

using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using SKYLAR_MANAGE;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Screens.General;
using SKYLAR_MANAGE.Screens.Suppliers;
using SKYLAR_MANAGE.Models;
using SKYLAR_MANAGE.Tools;
using SKYLAR_MANAGE.Screens.Stock;

namespace SKYLAR_MANAGE.Functions
{
    public static class MyExtensions
    {
        public enum Type
        {
            Supplier = 1,
            Customer = 2,
            Transaction =3,
            stock =4,
            Departments = 5,
           
        }



        public static void MyInitialize(this SearchLookUpEdit searchLookUpEdit, Type type,
            MyDbContext context, bool isRefresh = false, Func<bool, int> refreshList = null, bool showAddButton = true,
            bool loadData = true)
        {
            MyInitialize(null, searchLookUpEdit, type, context, isRefresh,
                refreshList, showAddButton, loadData);
        }


        private static void MyInitialize(RepositoryItemSearchLookUpEdit RsearchLookUpEdit = null,
            SearchLookUpEdit searchLookUpEdit = null, Type type = Type.Supplier,
            MyDbContext ctx = null, bool isRefresh = false, Func<bool, int> refreshList = null,
            bool showAddButton = true, bool loadData = true)
        {
           
            var isSearch = searchLookUpEdit != null;
            try
            {
                if (!isRefresh)
                {
                    var btn = new EditorButton(ButtonPredefines.Plus) { IsLeft = true };

                   
                     if (isSearch)
                    {
                        searchLookUpEdit.Properties.ShowAddNewButton = true;
                        searchLookUpEdit.Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Near;
                        searchLookUpEdit.Properties.NullText = "";
                    }

                    // Button event adds/edit rows
                    btn.Click += (_, _) =>
                    {
                        switch (type)
                        {
                            case Type.Supplier:
                            {
                               
                                var f = new AddSupplier();
                                f.ShowDialog();
                                refreshList?.Invoke(true);
                                break;
                            }
                            case Type.Departments:
                            //{


                            //}
                            

                            default:
                                throw new ArgumentOutOfRangeException(nameof(type), type, @"Not implemented");


                        }
                    };

                    // AddNewValue acts as refresh buttons
                   
                     if (isSearch)
                        searchLookUpEdit.Properties.AddNewValue += (_, _) => { refreshList?.Invoke(true); };

                    if (showAddButton)
                    {
                       
                         if (isSearch)
                            searchLookUpEdit.Properties.Buttons.Add(btn);
                    }
                }

                if (ctx == null)
                    return;

                switch (type)
                {
                    case Type.Supplier:
                    {
                        if (loadData)
                        {
                            var list = ctx.SuppliersAccounts
                                .OrderBy(s => s.SupplierId)
                                .ToList();

                          
                             if (isSearch)
                                searchLookUpEdit.Properties.DataSource = list;
                        }

                        if (!isRefresh)
                        {
                            var cols = new[]
                            {
                                new GridColumn
                                {
                                    Caption = nameof(SuppliersAccount.SupplierId).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(SuppliersAccount.SupplierId),
                                    Visible = true,
                                    VisibleIndex = 1
                                },
                                new GridColumn
                                {
                                    Caption = nameof(SuppliersAccount.SupplierName).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(SuppliersAccount.SupplierName),
                                    Visible = true,
                                    VisibleIndex = 2
                                },
                               
                            };

                           
                             if (isSearch)
                            {
                                searchLookUpEdit.Properties.ValueMember = nameof(SuppliersAccount.SupplierId);
                                searchLookUpEdit.Properties.DisplayMember = nameof(SuppliersAccount.SupplierName);
                                searchLookUpEdit.Properties.PopupView.Columns.Clear();
                                searchLookUpEdit.Properties.PopupView.Columns.AddRange(cols);
                            }
                        }

                        break;
                    }


                    case Type.Customer:
                        {
                            if (loadData)
                            {
                                var list = ctx.CustomersAccounts
                                    .OrderBy(s => s.CustomerId)
                                    .ToList();


                                if (isSearch)
                                    searchLookUpEdit.Properties.DataSource = list;
                            }

                            if (!isRefresh)
                            {
                                var cols = new[]
                                {
                                new GridColumn
                                {
                                    Caption = nameof(CustomersAccount.CustomerId).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(CustomersAccount.CustomerId),
                                    Visible = true,
                                    VisibleIndex = 1
                                },
                                new GridColumn
                                {
                                    Caption = nameof(CustomersAccount.CustomerName).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(CustomersAccount.CustomerName),
                                    Visible = true,
                                    VisibleIndex = 2
                                },

                            };


                                if (isSearch)
                                {
                                    searchLookUpEdit.Properties.ValueMember = nameof(CustomersAccount.CustomerId);
                                    searchLookUpEdit.Properties.DisplayMember = nameof(CustomersAccount.CustomerName);
                                    searchLookUpEdit.Properties.PopupView.Columns.Clear();
                                    searchLookUpEdit.Properties.PopupView.Columns.AddRange(cols);
                                }
                            }

                            break;
                        }

                    case Type.Transaction:
                        {
                            if (loadData)
                            {
                                var list = ctx.TransactionTypes
                                    .OrderBy(s => s.TranTypeId)
                                    .ToList();


                                if (isSearch)
                                    searchLookUpEdit.Properties.DataSource = list;
                            }

                            if (!isRefresh)
                            {
                                var cols = new[]
                                {
                                new GridColumn
                                {
                                    Caption = nameof(TransactionTypes.TranTypeId).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(TransactionTypes.TranTypeId),
                                    Visible = true,
                                    VisibleIndex = 1
                                },
                                new GridColumn
                                {
                                    Caption = nameof(TransactionTypes.TranTypeDesc).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(TransactionTypes.TranTypeDesc),
                                    Visible = true,
                                    VisibleIndex = 2
                                },

                            };


                                if (isSearch)
                                {
                                    searchLookUpEdit.Properties.ValueMember = nameof(TransactionTypes.TranTypeId);
                                    searchLookUpEdit.Properties.DisplayMember = nameof(TransactionTypes.TranTypeDesc);
                                    searchLookUpEdit.Properties.PopupView.Columns.Clear();
                                    searchLookUpEdit.Properties.PopupView.Columns.AddRange(cols);
                                }
                            }

                            break;
                        }
                    case Type.Departments:
                        {
                            if (loadData)
                            {
                                var list = ctx.Departments
                                  
                                    .ToList();


                                if (isSearch)
                                    searchLookUpEdit.Properties.DataSource = list;
                            }

                            if (!isRefresh)
                            {
                                var cols = new[]
                                {
                                new GridColumn
                                {
                                    Caption = nameof(Department.DepId).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(Department.DepId),
                                    Visible = true,
                                    VisibleIndex = 1
                                },
                                new GridColumn
                                {
                                    Caption = nameof(Department.DepName).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(Department.DepName),
                                    Visible = true,
                                    VisibleIndex = 2
                                },

                            };


                                if (isSearch)
                                {
                                    searchLookUpEdit.Properties.ValueMember = nameof(Department.DepId);
                                    searchLookUpEdit.Properties.DisplayMember = nameof(Department.DepName);
                                    searchLookUpEdit.Properties.PopupView.Columns.Clear();
                                    searchLookUpEdit.Properties.PopupView.Columns.AddRange(cols);
                                }
                            }

                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException(nameof(type), type, @"Not implemented");
                }

                if (!isRefresh)
                {
                    var screenSize = Screen.PrimaryScreen.Bounds;

                    var width = 70 * screenSize.Width / 100;

                 
                     if (isSearch)
                    {
                        searchLookUpEdit.Properties.ShowAddNewButton = true;
                        searchLookUpEdit.Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Near;
                        searchLookUpEdit.Properties.NullText = "";

                        if (searchLookUpEdit.Properties.PopupView.Columns.Count >= 3)
                            searchLookUpEdit.Properties.PopupFormWidth = width;
                    }
                }
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
            }
        }

        public static void MyInitialize2(this RepositoryItemSearchLookUpEdit ReposearchLookUpEdit, Type type,
            MyDbContext context, bool isRefresh = false, Func<bool, int> refreshList = null, bool showAddButton = true,
            bool loadData = true)
        {
            MyInitialize2(ReposearchLookUpEdit,null , type, context, isRefresh,
                refreshList, showAddButton, loadData);
        }


        private static void MyInitialize2(RepositoryItemSearchLookUpEdit ReposearchLookUpEdit = null,
            SearchLookUpEdit searchLookUpEdit = null, Type type = Type.Supplier,
            MyDbContext ctx = null, bool isRefresh = false, Func<bool, int> refreshList = null,
            bool showAddButton = true, bool loadData = true)
        {

            var isRepository = ReposearchLookUpEdit != null;
            try
            {
                if (!isRefresh)
                {
                    var btn = new EditorButton(ButtonPredefines.Plus) { IsLeft = true };


                    if (isRepository)
                    {
                      
                        ReposearchLookUpEdit.Appearance.TextOptions.HAlignment = HorzAlignment.Near;
                        ReposearchLookUpEdit.NullText = "";
                    }

                    // Button event adds/edit rows
                    btn.Click += (_, _) =>
                    {
                        switch (type)
                        {
                            case Type.Supplier:
                                {

                                    var f = new AddItem();
                                    f.ShowDialog();
                                    refreshList?.Invoke(true);
                                    break;
                                }
                            //case Type.Customer:
                            //{


                            //}


                            default:
                                throw new ArgumentOutOfRangeException(nameof(type), type, @"Not implemented");


                        }
                    };

                    // AddNewValue acts as refresh buttons

                    if (isRepository)
                        ReposearchLookUpEdit.AddNewValue += (_, _) => { refreshList?.Invoke(true); };

                  
                }

                if (ctx == null)
                    return;

                switch (type)
                {
                    case Type.stock:
                        {
                            if (loadData)
                            {
                                var list = ctx.Stocks
                                    .OrderBy(s => s.ItemBarcode)
                                    .ToList();


                                if (isRepository)
                                    ReposearchLookUpEdit.DataSource = list;
                            }

                            if (!isRefresh)
                            {
                                var cols = new[]
                                {
                                new GridColumn
                                {
                                    Caption = nameof(Stock.ItemBarcode).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(Stock.ItemBarcode),
                                    Visible = true,
                                    VisibleIndex = 1
                                },
                                new GridColumn
                                {
                                    Caption = nameof(Stock.ItemName).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(Stock.ItemName),
                                    Visible = true,
                                    VisibleIndex = 2
                                },

                            };


                                if (isRepository)
                                {
                                    ReposearchLookUpEdit.ValueMember = nameof(Stock.ItemBarcode);
                                    ReposearchLookUpEdit.DisplayMember = nameof(Stock.ItemName);
                                    ReposearchLookUpEdit.PopupView.Columns.Clear();
                                    ReposearchLookUpEdit.PopupView.Columns.AddRange(cols);
                                }
                            }

                            break;
                        }

                    case Type.Departments:
                        {
                            if (loadData)
                            {
                                var list = ctx.Departments
                                    
                                    .ToList();


                                if (isRepository)
                                    ReposearchLookUpEdit.DataSource = list;
                            }

                            if (!isRefresh)
                            {
                                var cols = new[]
                                {
                                new GridColumn
                                {
                                    Caption = nameof(Department.DepId).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(Department.DepId),
                                    Visible = true,
                                    VisibleIndex = 1
                                },
                                new GridColumn
                                {
                                    Caption = nameof(Department.DepName).UcWord().SplitOnCapitalLetter(),
                                    FieldName = nameof(Department.DepName),
                                    Visible = true,
                                    VisibleIndex = 2
                                },

                            };


                                if (isRepository)
                                {
                                    ReposearchLookUpEdit.ValueMember = nameof(Department.DepId);
                                    ReposearchLookUpEdit.DisplayMember = nameof(Department.DepName);
                                    ReposearchLookUpEdit.PopupView.Columns.Clear();
                                    ReposearchLookUpEdit.PopupView.Columns.AddRange(cols);
                                }
                            }

                            break;
                        }

                    default:
                        throw new ArgumentOutOfRangeException(nameof(type), type, @"Not implemented");
                }

                if (!isRefresh)
                {
                    var screenSize = Screen.PrimaryScreen.Bounds;

                    var width = 70 * screenSize.Width / 100;


                    if (isRepository)
                    {
                      
                        ReposearchLookUpEdit.Appearance.TextOptions.HAlignment = HorzAlignment.Near;
                        ReposearchLookUpEdit.NullText = "";

                        if (ReposearchLookUpEdit.PopupView.Columns.Count >= 3)
                            ReposearchLookUpEdit.PopupFormWidth = width;
                    }
                }
            }
            catch (Exception ex)
            {
                MyInformation.Msg.ShowErrorMessageWithLog(ex);
            }
        }
    }
}