﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoUpdaterDotNET;
using SKYLAR_MANAGE.Properties;

namespace SKYLAR_MANAGE.Functions
{
    public class MyUpdate
    {

        private bool _withMessage;
        public string UpdatePath;

        public MyUpdate()
        {
            AutoUpdater.ShowRemindLaterButton = false;
            AutoUpdater.ShowSkipButton = false;
            AutoUpdater.InstalledVersion = new Version(Application.ProductVersion);
            AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;
        }

        public void CheckUpdate(bool withMessage)
        {
            try
            {
                _withMessage = withMessage;

                var myPing = new Ping();
                var reply = myPing.Send(Settings.Default.DbIP, 2000);

                if (reply == null)
                {
                    MyInformation.IsCheckingUpdate = false;
                    return;
                }


                if (reply.Status != IPStatus.Success)
                {
                    MyInformation.IsCheckingUpdate = false;
                    return;
                }

                AutoUpdater.Start(Settings.Default.localftpUpdateServerPath);

            }
            catch (Exception)
            {
                MyInformation.IsCheckingUpdate = false;
                // close
            }
        }

        private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            if (args.Error == null)
            {
                if (args.IsUpdateAvailable)
                {
                    // Custom msg
                    DialogResult dialogResult;
                    if (args.Mandatory.Value)
                    {
                        dialogResult =
                            MessageBox.Show(
                                $@"There is a new version {args.CurrentVersion} Available.
You are Using Version {args.InstalledVersion}.
This is Required Update. Press Ok to begin Updating the Application.",
                                Properties.Resources.AppName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                    }
                    else
                    {
                        dialogResult =
                            MessageBox.Show(
                                $@"There is New Version {args.CurrentVersion} Available.
You are Using Version {args.InstalledVersion}.
Do you want to Update the Application Now ?", Properties.Resources.AppName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Information);
                    }

                    if (dialogResult.Equals(DialogResult.Yes) || dialogResult.Equals(DialogResult.OK))
                    {
                        try
                        {
                            if (AutoUpdater.DownloadUpdate(args))
                                Application.Exit();
                        }
                        catch (Exception exception)
                        { 
                            MessageBox.Show(exception.Message, exception.GetType().ToString(), MessageBoxButtons.OK,MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    if (_withMessage)
                        MessageBox.Show(
                            $"There is No Update Available. Please Check Later !",
                            Properties.Resources.AppName,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
              
                }
            }
            else
            {
             //   MessageBox.Show(args.Error.ToString(), Properties.Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (!_withMessage)
                    return;
                MessageBox.Show(
              "There is a Problem Reaching Server. Please Check Your Internet Connection and try again later !",
                    Properties.Resources.AppName,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
          
               
            }
        }
    }
}
