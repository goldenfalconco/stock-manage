﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeTecLib;
using SKYLAR_MANAGE.Properties;

namespace SKYLAR_MANAGE.Functions
{
    public class MyConnection
    {
        // Check connection with SQL server

        public static string CheckConnection()
        {
            try
            {
                var cn = new SqlConnection(ConString());

                cn.Open();

                return string.Empty;
            }
            catch (Exception ex)
            {
             
                return ex.Message;
            }
        }

        // SqlConnection string with/without database
        public static string ConString(bool withDb = true)
        {
            // user : sa = cwBhAA==
            // pass : dbpwd123+ = ZABiAHAAdwBkADEAMgAzACsA

            var builder = new SqlConnectionStringBuilder
            {
                UserID = CodeTecCodec.Base64Decode(Settings.Default.DbUser),
                Password = CodeTecCodec.Base64Decode(Settings.Default.DbPass),
                PersistSecurityInfo = false,
                DataSource = Settings.Default.DbIP,
                MultipleActiveResultSets = true,
                ConnectTimeout = 10000
            };

            builder.Add("App", "EntityFramework");

            if (withDb)
                builder.InitialCatalog = Settings.Default.DbName;

            return builder.ConnectionString;
        }


        // Reset the identity start increment of a table
        public static bool Reseed(string tableName, string startNb)
        {
            try
            {
                var query = $@"DBCC CHECKIDENT ('{tableName}', RESEED, {startNb})";

                RunTransaction(ConString(), query);

                return true;
            }
            catch (Exception ex)
            {
   
                return false;
            }
        }

        // Run any transaction
        public static int RunTransaction(string connection, string query,
            [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null)
        {
            Console.WriteLine($@"Line {lineNumber} ({caller})");
            var cn = new SqlConnection(connection);

            if (cn.State != System.Data.ConnectionState.Open)
                cn.Open();

            var cmd = cn.CreateCommand();

            // Start a local transaction
            var beginTransaction = cn.BeginTransaction();

            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            cmd.Connection = cn;
            cmd.CommandTimeout = 480;
            cmd.Transaction = beginTransaction;

            try
            {
                cmd.CommandText = query;
                var result = cmd.ExecuteNonQuery();
                beginTransaction.Commit();

                return result;
            }
            catch (Exception e)
            {
                try
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                        cn.Open();

                    beginTransaction.Rollback();
                }
                catch (SqlException ex)
                {
                    if (beginTransaction.Connection != null)
                    {
                        return -1;
                    }
                }

         
                return -1;
            }
            finally
            {
                if (cn.State == System.Data.ConnectionState.Open)
                    cn.Close();
            }
        }

        
    }
}
