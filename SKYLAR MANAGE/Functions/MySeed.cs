﻿using System;
using System.Windows.Forms;
using DevExpress.XtraExport.Xls;
using SKYLAR_MANAGE.Functions;
using SKYLAR_MANAGE.Properties;

namespace SKYLAR_MANAGE.Tools
{
    public static class MySeed
    {
        public static void SeedData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Cursor.Current = Cursors.Default;
                MessageBox.Show("Done", Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ex.Message, Resources.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}